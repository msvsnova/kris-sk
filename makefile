#------------------------------------------------------------------------------
#
# SuperNova APPLICATION - SK - MAKEFILE
# By FIST -  Kresimir Mandic 27.03.1999
#
#------------------------------------------------------------------------------

# Compiler options
#------------------------------------------------------------------------------
## CC          = start /w c:\snova\sncd\nova
CC          = start /w c:\vscd\bin\nova


CFLAGS      = -w -ereadfgl
#TD			= T:\sk\  
TD			= X:\sk\  
COMD        = .\lib\skcommon.fle 


SKFILES	= $(TD)sk.fle $(TD)sk_dd.fle    $(TD)klas.fle     $(TD)kam_naz.fle   \
	$(TD)kamstope.fle $(TD)l_klas.fle   $(TD)l_kamnaz.fle $(TD)l_kamst.fle   \
	$(TD)sk_pren.fle  $(TD)sk_pren2.fle $(TD)skpasswd.fle $(TD)chkdnkn.fle   \
	$(TD)vrskdok.fle  $(TD)sklock.fle   $(TD)sk_ngod.fle  $(TD)sk_a_dd.fle   \
    $(TD)skchkkum.fle $(TD)sk_flds.fle  $(TD)sk_brkif.fle $(TD)chk_pok.fle   \
    $(TD)pdv_matr.fle $(TD)vir_dd.fle   $(TD)r_virman.fle $(TD)skdbutl.fle  \
    $(TD)zimskrn.fle  $(TD)zimskup.fle  $(TD)sk_prstp.fle $(TD)sk_stp.fle    \
    $(TD)sk_stprn.fle $(TD)sk_stpup.fle $(TD)sk_stpdl.fle $(TD)vir_a_dd.fle  \
    $(TD)sk_unrac.fle $(TD)sk_unrns.fle $(TD)sk_punrn.fle $(TD)sk_unupl.fle  \
    $(TD)sk_prupl.fle $(TD)sk_unups.fle $(TD)pokrac.fle   $(TD)pokupl.fle    \
    $(TD)raskupl.fle  $(TD)l_dnknj.fle  $(TD)l_skrac.fle  $(TD)l_skupl.fle   \
    $(TD)prigots.fle  $(TD)ps_unrac.fle $(TD)ps_unrns.fle $(TD)ps_unupl.fle  \
    $(TD)ps_unups.fle $(TD)prigot.fle   $(TD)raskrac.fle  $(TD)izv_sk.fle    \
    $(TD)prifakt.fle  $(TD)prifakto.fle $(TD)priuzrn.fle  $(TD)priuzrno.fle  \
    $(TD)prirac.fle   $(TD)priotp.fle   $(TD)priotps.fle  $(TD)pristprn.fle  \
    $(TD)sk_prpis.fle $(TD)sk_zbir.fle  $(TD)l_skpo.fle   $(TD)l_skkar.fle   \
    $(TD)l_skzbir.fle $(TD)ios.fle      $(TD)sk_start.fle $(TD)pr_unups.fle  \
    $(TD)l_skvdok.fle $(TD)rac_upd.fle  $(TD)upl_upd.fle  $(TD)l_vispec.fle  \
    $(TD)viewpok.fle  $(TD)findppar.fle $(TD)sk_rnvir.fle $(TD)delpokr.fle   \
    $(TD)sk_pojed.fle $(TD)sk_avirm.fle $(TD)zbir_isp.fle $(TD)sk_virm.fle   \
    $(TD)sk_virms.fle $(TD)get_vir.fle  $(TD)get_virs.fle $(TD)l_prpok.fle   \
    $(TD)l_racpdv.fle $(TD)kifkuf.fle   $(TD)lkif.fle     $(TD)lkuf.fle      \
    $(TD)sk_obrkt.fle $(TD)l_racprp.fle $(TD)obrkam1.fle  $(TD)obrkam2.fle   \
    $(TD)l_kampo.fle  $(TD)lsk_kam.fle  $(TD)sk_dopzb.fle $(TD)lskdopzb.fle  \
    $(TD)lioszag.fle  $(TD)lioskar.fle  $(TD)pdv_zag.fle  $(TD)pdv_stav.fle  \
    $(TD)lpdv_obr.fle $(TD)sk_lstr2.fle $(TD)lsklstr2.fle $(TD)priborn.fle   \
    $(TD)priborno.fle $(TD)lopomzag.fle $(TD)lopomkar.fle $(TD)sktecraz.fle  \
    $(TD)l_tecraz.fle $(TD)sk_komzb.fle $(TD)lskkomzb.fle $(TD)lskkompo.fle  \
    $(TD)skorgnik.fle $(TD)skpnmkif.fle $(TD)skrndosp.fle $(TD)lskrndosp.fle \
    $(TD)priacpv.fle  $(TD)sk_lovir.fle $(TD)sk_unkif.fle $(TD)skdilgar.fle  \
    $(TD)sk_unkuf.fle $(TD)lsklstr2old.fle $(TD)lkufac.fle $(TD)lac_kam.fle  \
    $(TD)l_tecraz_sr.fle $(TD)l_tecraz_su.fle $(TD)prirnser.fle $(TD)prifakt_gt.fle\
    $(TD)sk_osig.fle  $(TD)skprenoff.fle $(TD)skprencb.fle $(TD)l_bjskkar.fle   \
    $(TD)sk_hrkomp.fle $(TD)sk_idx.fle  $(TD)lnk_rpi.fle  $(TD)lsk_nzktms.fle   \
    $(TD)sk_izvjop.fle $(TD)l_rktraz.fle $(TD)lac_kam1.fle $(TD)sk_default.fle  \
    $(TD)skrnndosp.fle $(TD)lskrnndosp.fle $(TD)gt_unrns.fle $(TD)gt_punrn.fle  \
    $(TD)skchk_gk_sk.fle $(TD)prirn_vw.fle $(TD)dacvw_dd.fle $(TD)prigot_vw.fle \
    $(TD)klasif_prst.fle $(TD)prigots_vw.fle $(TD)lklas_prst.fle $(TD)prifakto_gt.fle \
    $(TD)niveliranje.fle $(TD)kompenzacije.fle $(TD)lsk_komp.fle $(TD)lioszag_vd.fle  \
    $(TD)lopomzag_vd.fle $(TD)skkomp_arh.fle $(TD)prifakt_nzf.fle  $(TD)prifakto_nzf.fle \
    $(TD)avansi.fle $(TD)sk_prrac_vw.fle $(TD)sk_prgot_vw.fle $(TD)sk_prprm_vw.fle   \
    $(TD)sk_protp_vw.fle $(TD)prigot_voz.fle $(TD)prigots_voz.fle $(TD)virpon.fle    \
    $(TD)reg_rnvir.fle $(TD)l_vpdosp.fle $(TD)reg_virm.fle $(TD)reg_virms.fle        \
    $(TD)reg_avirm.fle $(TD)sk_kif_ef3.fle $(TD)imp_kifbr.fle $(TD)kif_efcom.fle     \
    $(TD)pribgot_vw.fle $(TD)pribgots_vw.fle $(TD)skkam_fakt.fle $(TD)skrnnedosp.fle \
    $(TD)jpriuzrn.fle  $(TD)jpriuzrno.fle $(TD)lopomzagt.fle $(TD)lopomzag_vdt.fle   \
    $(TD)prifakt_rf.fle $(TD)sk_tecaj_hnb.fle $(TD)view_prst.fle $(TD)crosstokris.fle  \
    $(TD)prirn_cross.fle $(TD)sk_rev.fle $(TD)epdv_mp.fle $(TD)lkif11.fle  $(TD)lkuf11.fle \
    $(TD)fillkif.fle  $(TD)kif.fle       $(TD)racfromziro.fle $(TD)sk_auto_pok.fle \
    $(TD)genddx1.fle  $(TD)lpdvk_obr.fle $(TD)sve_pokrij.fle  $(TD)lsve_pokrij.fle 

SKFILES2  = $(TD)pdv_zag2.fle $(TD)lkif_eu.fle   $(TD)l_pdvs.fle $(TD)pdvs.fle $(TD)pdv_zag3.fle \
    $(TD)pdvzp.fle $(TD)l_pdvzp.fle $(TD)lkuf_eu.fle $(TD)lufa_pdvs.fle $(TD)pdvs_zag.fle \
    $(TD)pdvs_stav.fle $(TD)zp_zag.fle $(TD)zp_stav.fle $(TD)lkuf2014.fle $(TD)lkif2014.fle \
    $(TD)pdv_datumi.fle $(TD)list_pnb.fle $(TD)ppo_zbirno.fle $(TD)l_ppo_zbirno.fle \
    $(TD)view_vanbil.fle  $(TD)l_view_vb.fle $(TD)lsk_kam_rek.fle $(TD)lopomhpa.fle \
    $(TD)l_pokontima.fle $(TD)pdf_opom.fle $(TD)ufa_pdvs.fle  $(TD)view_pdvs.fle  \
    $(TD)stat_izvj.fle  $(TD)pdv_zag3.fle $(TD)opz_stat.fle $(TD)opz_stat_rac.fle \
    $(TD)lopz_storna.fle $(TD)opz_ozn_dok.fle $(TD)lopz_stat.fle $(TD)lrek_po_kto.fle \
    $(TD)sk_ugovori.fle $(TD)sk_ugovori_st.fle $(TD)lsk_ugovor.fle $(TD)hpa_opom.fle  \
    $(TD)lopomhpa_vd.fle $(TD)ppo_15.fle $(TD)l_ppo_15.fle $(TD)ozn_dok_err.fle $(TD)lozn_dok_err.fle \
    $(TD)lioszaghpa.fle $(TD)lioszaghpa_vd.fle $(TD)lopz_err.fle $(TD)l_skkar_one.fle \
    $(TD)hpa_ios.fle $(TD)ura_xml.fle $(TD)r2_racuni.fle $(TD)akj_dd.fle $(TD)akj_load.fle \
    $(TD)ak_racuni.fle $(TD)akj_racuni_st.fle $(TD)l_akrnnpl.fle $(TD)akj_ugrupe.fle $(TD)akj_grupe.fle 

MILKFILES = $(TD)milk.fle $(TD)milk_dd.fle  $(TD)milk_a_dd.fle \
    $(TD)milk_fact.fle $(TD)milk_prod.fle $(TD)sab_mjesta.fle  \
    $(TD)l_milk_fact.fle $(TD)l_sab_mjesta.fle $(TD)l_milk_prod.fle  \
    $(TD)milk_kateg.fle $(TD)l_milk_kateg.fle $(TD)l_milk_koefzn.fle  \
    $(TD)milk_koefzn.fle $(TD)hssc_milk.fle $(TD)primilk_rac.fle $(TD)milk_rac.fle  \
    $(TD)l_milk_rac.fle $(TD)milk_racstat.fle $(TD)lmilk_racstat.fle  \
    $(TD)milk_sifra.fle $(TD)milk_usluga.fle  $(TD)l_milk_sifra.fle   \
    $(TD)milkdbutl.fle $(TD)l_milk_usluga.fle $(TD)primilk_pot.fle $(TD)milk_potsif.fle \
    $(TD)milk_nalfakt.fle $(TD)lmilk_cesija.fle $(TD)milk_count.fle $(TD)milk_statkor.fle \
    $(TD)lmilk_nalppar.fle $(TD)milk_kpot.fle $(TD)farme_dug.fle $(TD)milk_star.fle

COMMON   =  dacvw_dd.ls izv_sk.ls   klas.ls     milk_dd.ls milk_ddx.ls  pdv_matr.ls \
            pr_unups.ls priuzrno.ls r_virman.ls sk_dd.ls   sk_ddx.ls    sk_flds.ls  \
            sk_prpis.ls sk_unrac.ls sk_unupl.ls skpasswd.ls vir_dd.ls vir_ddx.ls  \
            vrskdok.ls sk_osig.ls



## $(TD)opca_upl.fle  

all     :  $(SKFILES) $(SKFILES2) $(MILKFILES)

############### COMMON
r_virman.ls:  r_virman.lgc
    $(CC) $(CFLAGS) appl=r_virman dotfle=$(COMD)
    @echo " ">r_virman.ls

sk_dd.ls:  sk_dd.lgc
    $(CC) $(CFLAGS) appl=sk_dd dotfle=.\lib\sk_dd.fle 
    @echo " ">sk_dd.ls

sk_ddx.ls:  sk_ddx.lgc
    $(CC) $(CFLAGS) appl=sk_ddx dotfle=.\lib\sk_dd.fle 
    @echo " ">sk_ddx.ls

sk_flds.ls:  sk_flds.lgc
    $(CC) $(CFLAGS) appl=sk_flds dotfle=.\lib\sk_dd.fle 
    @echo " ">sk_flds.ls

sk_prpis.ls:  sk_prpis.lgc
    $(CC) $(CFLAGS) appl=sk_prpis dotfle=$(COMD)
    @echo " ">sk_prpis.ls

sk_unrac.ls:  sk_unrac.lgc
    $(CC) $(CFLAGS) appl=sk_unrac dotfle=$(COMD)
    @echo " ">sk_unrac.ls

sk_unupl.ls:  sk_unupl.lgc
    $(CC) $(CFLAGS) appl=sk_unupl dotfle=$(COMD)
    @echo " ">sk_unupl.ls

skpasswd.ls:  skpasswd.lgc
    $(CC) $(CFLAGS) appl=skpasswd dotfle=$(COMD)
    @echo " ">skpasswd.ls

vir_dd.ls:  vir_dd.lgc
    $(CC) $(CFLAGS) appl=vir_dd dotfle=.\lib\sk_dd.fle 
    @echo " ">vir_dd.ls

vir_ddx.ls:  vir_ddx.lgc
    $(CC) $(CFLAGS) appl=vir_ddx dotfle=.\lib\sk_dd.fle 
    @echo " ">vir_ddx.ls

vrskdok.ls:  vrskdok.lgc
    $(CC) $(CFLAGS) appl=vrskdok dotfle=$(COMD)
    @echo " ">vrskdok.ls

sk_osig.ls:  sk_osig.lgc
    $(CC) $(CFLAGS) appl=sk_osig dotfle=$(COMD)
    @echo " ">sk_osig.ls

#################################################
dacvw_dd.ls:  dacvw_dd.lgc
    $(CC) $(CFLAGS) appl=dacvw_dd dotfle=.\lib\sk_dd.fle 
    @echo " ">dacvw_dd.ls

izv_sk.ls:  izv_sk.lgc
    $(CC) $(CFLAGS) appl=izv_sk dotfle=$(COMD)
    @echo " ">izv_sk.ls

klas.ls:  klas.lgc
    $(CC) $(CFLAGS) appl=klas dotfle=$(COMD)
    @echo " ">klas.ls

milk_dd.ls:  milk_dd.lgc
    $(CC) $(CFLAGS) appl=milk_dd dotfle=.\lib\sk_dd.fle 
    @echo " ">milk_dd.ls

milk_ddx.ls:  milk_ddx.lgc
    $(CC) $(CFLAGS) appl=milk_ddx dotfle=.\lib\sk_dd.fle 
    @echo " ">milk_ddx.ls

pdv_matr.ls:  pdv_matr.lgc
    $(CC) $(CFLAGS) appl=pdv_matr dotfle=$(COMD)
    @echo " ">pdv_matr.ls

pr_unups.ls:  pr_unups.lgc
    $(CC) $(CFLAGS) appl=pr_unups dotfle=$(COMD)
    @echo " ">pr_unups.ls

priuzrno.ls:  priuzrno.lgc
    $(CC) $(CFLAGS) appl=priuzrno dotfle=$(COMD)
    @echo " ">priuzrno.ls



########################################################
#
#  dd-ovi
#
########################################################
$(TD)sk_dd.fle:  sk_dd.lgc
    $(CC) $(CFLAGS) appl=sk_dd dotfle=$(TD)sk_dd.fle
    $(CC) -w genddx ddfile=sk_dd
    $(CC) $(CFLAGS) appl=sk_ddx dotfle=$(TD)sk_ddx.fle

$(TD)vir_dd.fle:  vir_dd.lgc
    $(CC) $(CFLAGS) appl=vir_dd dotfle=$(TD)vir_dd.fle
    $(CC) -w genddx ddfile=vir_dd
    $(CC) $(CFLAGS) appl=vir_ddx dotfle=$(TD)sk_dd.fle

$(TD)skdbutl.fle:  skdbutl.lgc
    $(CC) $(CFLAGS) appl=skdbutl dotfle=$(TD)skdbutl.fle
    @echo " ">skdbutl.ts

$(TD)milk_dd.fle:  milk_dd.lgc
    $(CC) $(CFLAGS) appl=milk_dd dotfle=$(TD)milk_dd.fle
    $(CC) -w genddx ddfile=milk_dd
    $(CC) $(CFLAGS) appl=milk_ddx dotfle=$(TD)milk_ddx.fle

$(TD)akj_dd.fle:  akj_dd.lgc
    $(CC) $(CFLAGS) appl=akj_dd dotfle=$(TD)akj_dd.fle
    $(CC) -w genddx ddfile=akj_dd
    $(CC) $(CFLAGS) appl=akj_ddx dotfle=$(TD)akj_ddx.fle

########################################################
#
#  MPSY
#
########################################################

$(TD)sk_flds.fle:  sk_flds.lgc
    $(CC) $(CFLAGS) appl=sk_flds dotfle=$(TD)sk_flds.fle

$(TD)sk.fle:  sk.lgc
    $(CC) $(CFLAGS) appl=sk dotfle=$(TD)sk.fle

$(TD)klas.fle:  klas.lgc
    $(CC) $(CFLAGS) appl=klas dotfle=$(TD)klas.fle
    
$(TD)vrskdok.fle:  vrskdok.lgc
    $(CC) $(CFLAGS) appl=vrskdok dotfle=$(TD)vrskdok.fle
    
$(TD)kam_naz.fle:  kam_naz.lgc
    $(CC) $(CFLAGS) appl=kam_naz dotfle=$(TD)kam_naz.fle

$(TD)kamstope.fle:  kamstope.lgc
    $(CC) $(CFLAGS) appl=kamstope dotfle=$(TD)kamstope.fle

$(TD)l_klas.fle:  l_klas.lgc
    $(CC) $(CFLAGS) appl=l_klas dotfle=$(TD)l_klas.fle

$(TD)l_kamnaz.fle:  l_kamnaz.lgc
    $(CC) $(CFLAGS) appl=l_kamnaz dotfle=$(TD)l_kamnaz.fle

$(TD)l_kamst.fle:  l_kamst.lgc
    $(CC) $(CFLAGS) appl=l_kamst dotfle=$(TD)l_kamst.fle

$(TD)sk_pren.fle:  sk_pren.lgc
    $(CC) $(CFLAGS) appl=sk_pren dotfle=$(TD)sk_pren.fle

$(TD)sk_pren2.fle:  sk_pren2.lgc
    $(CC) $(CFLAGS) appl=sk_pren2 dotfle=$(TD)sk_pren2.fle

$(TD)skpasswd.fle:  skpasswd.lgc
    $(CC) $(CFLAGS) appl=skpasswd dotfle=$(TD)skpasswd.fle
    
$(TD)chkdnkn.fle:  chkdnkn.lgc
    $(CC) $(CFLAGS) appl=chkdnkn dotfle=$(TD)chkdnkn.fle
    
$(TD)sklock.fle:  sklock.lgc
    $(CC) $(CFLAGS) appl=sklock dotfle=$(TD)sklock.fle
    
$(TD)sk_ngod.fle:  sk_ngod.lgc
    $(CC) $(CFLAGS) appl=sk_ngod dotfle=$(TD)sk_ngod.fle
    
$(TD)sk_a_dd.fle:  sk_a_dd.lgc
    $(CC) $(CFLAGS) appl=sk_a_dd dotfle=$(TD)sk_a_dd.fle
    
$(TD)vir_a_dd.fle:  vir_a_dd.lgc
    $(CC) $(CFLAGS) appl=vir_a_dd dotfle=$(TD)vir_a_dd.fle
    
$(TD)skchkkum.fle:  skchkkum.lgc
    $(CC) $(CFLAGS) appl=skchkkum dotfle=$(TD)skchkkum.fle
    
$(TD)sk_brkif.fle:  sk_brkif.lgc
    $(CC) $(CFLAGS) appl=sk_brkif dotfle=$(TD)sk_brkif.fle

$(TD)chk_pok.fle:  chk_pok.lgc
    $(CC) $(CFLAGS) appl=chk_pok dotfle=$(TD)chk_pok.fle

$(TD)pdv_matr.fle:  pdv_matr.lgc
    $(CC) $(CFLAGS) appl=pdv_matr dotfle=$(TD)pdv_matr.fle

$(TD)r_virman.fle:  r_virman.lgc
    $(CC) $(CFLAGS) appl=r_virman dotfle=$(TD)r_virman.fle

$(TD)zimskrn.fle:  zimskrn.lgc
    $(CC) $(CFLAGS) appl=zimskrn dotfle=$(TD)zimskrn.fle

$(TD)zimskup.fle:  zimskup.lgc
    $(CC) $(CFLAGS) appl=zimskup dotfle=$(TD)zimskup.fle

$(TD)sk_prstp.fle:  sk_prstp.lgc
    $(CC) $(CFLAGS) appl=sk_prstp dotfle=$(TD)sk_prstp.fle

$(TD)sk_stp.fle:  sk_stp.lgc
    $(CC) $(CFLAGS) appl=sk_stp dotfle=$(TD)sk_stp.fle

$(TD)sk_stprn.fle:  sk_stprn.lgc
    $(CC) $(CFLAGS) appl=sk_stprn dotfle=$(TD)sk_stprn.fle

$(TD)sk_stpup.fle:  sk_stpup.lgc
    $(CC) $(CFLAGS) appl=sk_stpup dotfle=$(TD)sk_stpup.fle

$(TD)sk_stpdl.fle:  sk_stpdl.lgc
    $(CC) $(CFLAGS) appl=sk_stpdl dotfle=$(TD)sk_stpdl.fle

$(TD)skorgnik.fle:  skorgnik.lgc
    $(CC) $(CFLAGS) appl=skorgnik dotfle=$(TD)skorgnik.fle

$(TD)skpnmkif.fle:  skpnmkif.lgc
    $(CC) $(CFLAGS) appl=skpnmkif dotfle=$(TD)skpnmkif.fle

########################################################
#
#  PROM
#
########################################################

$(TD)sk_unrac.fle:  sk_unrac.lgc
    $(CC) $(CFLAGS) appl=sk_unrac dotfle=$(TD)sk_unrac.fle

$(TD)sk_unrns.fle:  sk_unrns.lgc
    $(CC) $(CFLAGS) appl=sk_unrns dotfle=$(TD)sk_unrns.fle

$(TD)ps_unrac.fle:  ps_unrac.lgc
    $(CC) $(CFLAGS) appl=ps_unrac dotfle=$(TD)ps_unrac.fle

$(TD)ps_unrns.fle:  ps_unrns.lgc
    $(CC) $(CFLAGS) appl=ps_unrns dotfle=$(TD)ps_unrns.fle

$(TD)sk_punrn.fle:  sk_punrn.lgc
    $(CC) $(CFLAGS) appl=sk_punrn dotfle=$(TD)sk_punrn.fle

$(TD)sk_unupl.fle:  sk_unupl.lgc
    $(CC) $(CFLAGS) appl=sk_unupl dotfle=$(TD)sk_unupl.fle

$(TD)sk_unups.fle:  sk_unups.lgc
    $(CC) $(CFLAGS) appl=sk_unups dotfle=$(TD)sk_unups.fle

$(TD)pr_unups.fle:  pr_unups.lgc
    $(CC) $(CFLAGS) appl=pr_unups dotfle=$(TD)pr_unups.fle

$(TD)ps_unupl.fle:  ps_unupl.lgc
    $(CC) $(CFLAGS) appl=ps_unupl dotfle=$(TD)ps_unupl.fle

$(TD)ps_unups.fle:  ps_unups.lgc
    $(CC) $(CFLAGS) appl=ps_unups dotfle=$(TD)ps_unups.fle

$(TD)izv_sk.fle:  izv_sk.lgc
    $(CC) $(CFLAGS) appl=izv_sk dotfle=$(TD)izv_sk.fle

$(TD)sk_prupl.fle:  sk_prupl.lgc
    $(CC) $(CFLAGS) appl=sk_prupl dotfle=$(TD)sk_prupl.fle

$(TD)pokrac.fle:  pokrac.lgc
    $(CC) $(CFLAGS) appl=pokrac dotfle=$(TD)pokrac.fle

$(TD)pokupl.fle:  pokupl.lgc
    $(CC) $(CFLAGS) appl=pokupl dotfle=$(TD)pokupl.fle

$(TD)raskrac.fle:  raskrac.lgc
    $(CC) $(CFLAGS) appl=raskrac dotfle=$(TD)raskrac.fle

$(TD)raskupl.fle:  raskupl.lgc
    $(CC) $(CFLAGS) appl=raskupl dotfle=$(TD)raskupl.fle

$(TD)l_dnknj.fle:  l_dnknj.lgc
    $(CC) $(CFLAGS) appl=l_dnknj dotfle=$(TD)l_dnknj.fle

$(TD)l_skrac.fle:  l_skrac.lgc
    $(CC) $(CFLAGS) appl=l_skrac dotfle=$(TD)l_skrac.fle

$(TD)l_skupl.fle:  l_skupl.lgc
    $(CC) $(CFLAGS) appl=l_skupl dotfle=$(TD)l_skupl.fle

$(TD)prigot.fle:  prigot.lgc
    $(CC) $(CFLAGS) appl=prigot dotfle=$(TD)prigot.fle

$(TD)prigots.fle:  prigots.lgc
    $(CC) $(CFLAGS) appl=prigots dotfle=$(TD)prigots.fle

$(TD)priotp.fle:  priotp.lgc
    $(CC) $(CFLAGS) appl=priotp dotfle=$(TD)priotp.fle

$(TD)priotps.fle:  priotps.lgc
    $(CC) $(CFLAGS) appl=priotps dotfle=$(TD)priotps.fle

$(TD)pristprn.fle:  pristprn.lgc
    $(CC) $(CFLAGS) appl=pristprn dotfle=$(TD)pristprn.fle

$(TD)prifakt.fle:  prifakt.lgc
    $(CC) $(CFLAGS) appl=prifakt dotfle=$(TD)prifakt.fle

$(TD)prifakt_rf.fle:  prifakt_rf.lgc
    $(CC) $(CFLAGS) appl=prifakt_rf dotfle=$(TD)prifakt_rf.fle

$(TD)prifakt_nzf.fle:  prifakt_nzf.lgc
    $(CC) $(CFLAGS) appl=prifakt_nzf dotfle=$(TD)prifakt_nzf.fle

$(TD)prifakto.fle:  prifakto.lgc
    $(CC) $(CFLAGS) appl=prifakto dotfle=$(TD)prifakto.fle

$(TD)prifakto_nzf.fle:  prifakto_nzf.lgc
    $(CC) $(CFLAGS) appl=prifakto_nzf dotfle=$(TD)prifakto_nzf.fle

$(TD)avansi.fle:  avansi.lgc
    $(CC) $(CFLAGS) appl=avansi dotfle=$(TD)avansi.fle

$(TD)prifakt_gt.fle:  prifakt_gt.lgc
    $(CC) $(CFLAGS) appl=prifakt_gt dotfle=$(TD)prifakt_gt.fle

$(TD)priuzrn.fle:  priuzrn.lgc
    $(CC) $(CFLAGS) appl=priuzrn dotfle=$(TD)priuzrn.fle

$(TD)priuzrno.fle:  priuzrno.lgc
    $(CC) $(CFLAGS) appl=priuzrno dotfle=$(TD)priuzrno.fle

$(TD)prirac.fle:  prirac.lgc
    $(CC) $(CFLAGS) appl=prirac dotfle=$(TD)prirac.fle

$(TD)priborn.fle:  priborn.lgc
    $(CC) $(CFLAGS) appl=priborn dotfle=$(TD)priborn.fle

$(TD)priborno.fle:  priborno.lgc
    $(CC) $(CFLAGS) appl=priborno dotfle=$(TD)priborno.fle

$(TD)priacpv.fle:  priacpv.lgc
    $(CC) $(CFLAGS) appl=priacpv dotfle=$(TD)priacpv.fle

$(TD)prirnser.fle:  prirnser.lgc
    $(CC) $(CFLAGS) appl=prirnser dotfle=$(TD)prirnser.fle

$(TD)skprenoff.fle:  skprenoff.lgc
    $(CC) $(CFLAGS) appl=skprenoff dotfle=$(TD)skprenoff.fle

$(TD)skprencb.fle:  skprencb.lgc
    $(CC) $(CFLAGS) appl=skprencb dotfle=$(TD)skprencb.fle

$(TD)sk_osig.fle:  sk_osig.lgc
    $(CC) $(CFLAGS) appl=sk_osig dotfle=$(TD)sk_osig.fle

$(TD)jpriuzrn.fle:  jpriuzrn.lgc
    $(CC) $(CFLAGS) appl=jpriuzrn dotfle=$(TD)jpriuzrn.fle

$(TD)jpriuzrno.fle:  jpriuzrno.lgc
    $(CC) $(CFLAGS) appl=jpriuzrno dotfle=$(TD)jpriuzrno.fle

########################################################
#
#  IZVJE��A
#
########################################################

$(TD)sk_prpis.fle:  sk_prpis.lgc
    $(CC) $(CFLAGS) appl=sk_prpis dotfle=$(TD)sk_prpis.fle

$(TD)sk_pojed.fle:  sk_pojed.lgc
    $(CC) $(CFLAGS) appl=sk_pojed dotfle=$(TD)sk_pojed.fle

$(TD)rac_upd.fle:  rac_upd.lgc
    $(CC) $(CFLAGS) appl=rac_upd dotfle=$(TD)rac_upd.fle

$(TD)upl_upd.fle:  upl_upd.lgc
    $(CC) $(CFLAGS) appl=upl_upd dotfle=$(TD)upl_upd.fle

$(TD)viewpok.fle:  viewpok.lgc
    $(CC) $(CFLAGS) appl=viewpok dotfle=$(TD)viewpok.fle

$(TD)sk_zbir.fle:  sk_zbir.lgc
    $(CC) $(CFLAGS) appl=sk_zbir dotfle=$(TD)sk_zbir.fle

$(TD)findppar.fle:  findppar.lgc
    $(CC) $(CFLAGS) appl=findppar dotfle=$(TD)findppar.fle

$(TD)zbir_isp.fle:  zbir_isp.lgc
    $(CC) $(CFLAGS) appl=zbir_isp dotfle=$(TD)zbir_isp.fle

$(TD)l_skkar.fle:  l_skkar.lgc
    $(CC) $(CFLAGS) appl=l_skkar dotfle=$(TD)l_skkar.fle

$(TD)l_skkar_one.fle:  l_skkar_one.lgc
    $(CC) $(CFLAGS) appl=l_skkar_one dotfle=$(TD)l_skkar_one.fle

$(TD)l_bjskkar.fle:  l_bjskkar.lgc
    $(CC) $(CFLAGS) appl=l_bjskkar dotfle=$(TD)l_bjskkar.fle

$(TD)l_skpo.fle:  l_skpo.lgc
    $(CC) $(CFLAGS) appl=l_skpo dotfle=$(TD)l_skpo.fle

$(TD)l_skzbir.fle:  l_skzbir.lgc
    $(CC) $(CFLAGS) appl=l_skzbir dotfle=$(TD)l_skzbir.fle

$(TD)kamate.fle:  kamate.lgc
    $(CC) $(CFLAGS) appl=kamate dotfle=$(TD)kamate.fle

$(TD)ios.fle:  ios.lgc
    $(CC) $(CFLAGS) appl=ios dotfle=$(TD)ios.fle

$(TD)l_skvdok.fle:  l_skvdok.lgc
    $(CC) $(CFLAGS) appl=l_skvdok dotfle=$(TD)l_skvdok.fle
    
$(TD)delpokr.fle:  delpokr.lgc
    $(CC) $(CFLAGS) appl=delpokr dotfle=$(TD)delpokr.fle
    
$(TD)sk_rnvir.fle:  sk_rnvir.lgc
    $(CC) $(CFLAGS) appl=sk_rnvir dotfle=$(TD)sk_rnvir.fle
    
$(TD)sk_virm.fle:  sk_virm.lgc
    $(CC) $(CFLAGS) appl=sk_virm dotfle=$(TD)sk_virm.fle
    
$(TD)sk_virms.fle:  sk_virms.lgc
    $(CC) $(CFLAGS) appl=sk_virms dotfle=$(TD)sk_virms.fle
    
$(TD)l_vispec.fle:  l_vispec.lgc
    $(CC) $(CFLAGS) appl=l_vispec dotfle=$(TD)l_vispec.fle
    
$(TD)sk_avirm.fle:  sk_avirm.lgc
    $(CC) $(CFLAGS) appl=sk_avirm dotfle=$(TD)sk_avirm.fle
    
$(TD)get_vir.fle:  get_vir.lgc
    $(CC) $(CFLAGS) appl=get_vir dotfle=$(TD)get_vir.fle
    
$(TD)get_virs.fle:  get_virs.lgc
    $(CC) $(CFLAGS) appl=get_virs dotfle=$(TD)get_virs.fle
    
$(TD)l_prpok.fle:  l_prpok.lgc
    $(CC) $(CFLAGS) appl=l_prpok dotfle=$(TD)l_prpok.fle
    
$(TD)kifkuf.fle:  kifkuf.lgc
    $(CC) $(CFLAGS) appl=kifkuf dotfle=$(TD)kifkuf.fle
    
$(TD)lkif.fle:  lkif.lgc
    $(CC) $(CFLAGS) appl=lkif dotfle=$(TD)lkif.fle
    
$(TD)lkif_eu.fle:  lkif_eu.lgc
    $(CC) $(CFLAGS) appl=lkif_eu dotfle=$(TD)lkif_eu.fle
    
$(TD)lkuf_eu.fle:  lkuf_eu.lgc
    $(CC) $(CFLAGS) appl=lkuf_eu dotfle=$(TD)lkuf_eu.fle
    
$(TD)lkuf2014.fle:  lkuf2014.lgc
    $(CC) $(CFLAGS) appl=lkuf2014 dotfle=$(TD)lkuf2014.fle
    
$(TD)lkif2014.fle:  lkif2014.lgc
    $(CC) $(CFLAGS) appl=lkif2014 dotfle=$(TD)lkif2014.fle
    
$(TD)lufa_pdvs.fle:  lufa_pdvs.lgc
    $(CC) $(CFLAGS) appl=lufa_pdvs dotfle=$(TD)lufa_pdvs.fle
    
$(TD)l_pdvs.fle:  l_pdvs.lgc
    $(CC) $(CFLAGS) appl=l_pdvs dotfle=$(TD)l_pdvs.fle
    
$(TD)pdvs.fle:  pdvs.lgc
    $(CC) $(CFLAGS) appl=pdvs dotfle=$(TD)pdvs.fle
    
$(TD)pdvzp.fle:  pdvzp.lgc
    $(CC) $(CFLAGS) appl=pdvzp dotfle=$(TD)pdvzp.fle
    
$(TD)l_pdvzp.fle:  l_pdvzp.lgc
    $(CC) $(CFLAGS) appl=l_pdvzp dotfle=$(TD)l_pdvzp.fle
    
$(TD)lkuf.fle:  lkuf.lgc
    $(CC) $(CFLAGS) appl=lkuf dotfle=$(TD)lkuf.fle

$(TD)lkif11.fle:  lkif11.lgc
    $(CC) $(CFLAGS) appl=lkif11 dotfle=$(TD)lkif11.fle
    
$(TD)lkuf11.fle:  lkuf11.lgc
    $(CC) $(CFLAGS) appl=lkuf11 dotfle=$(TD)lkuf11.fle

$(TD)obrkam1.fle:  obrkam1.lgc
    $(CC) $(CFLAGS) appl=obrkam1 dotfle=$(TD)obrkam1.fle

$(TD)obrkam2.fle:  obrkam2.lgc
    $(CC) $(CFLAGS) appl=obrkam2 dotfle=$(TD)obrkam2.fle

$(TD)l_kampo.fle:  l_kampo.lgc
    $(CC) $(CFLAGS) appl=l_kampo dotfle=$(TD)l_kampo.fle

$(TD)sk_obrkt.fle:  sk_obrkt.lgc
    $(CC) $(CFLAGS) appl=sk_obrkt dotfle=$(TD)sk_obrkt.fle

$(TD)lsk_kam.fle:  lsk_kam.lgc
    $(CC) $(CFLAGS) appl=lsk_kam dotfle=$(TD)lsk_kam.fle

$(TD)lac_kam.fle:  lac_kam.lgc
    $(CC) $(CFLAGS) appl=lac_kam dotfle=$(TD)lac_kam.fle

$(TD)lac_kam1.fle:  lac_kam1.lgc
    $(CC) $(CFLAGS) appl=lac_kam1 dotfle=$(TD)lac_kam1.fle

$(TD)l_racpdv.fle:  l_racpdv.lgc
    $(CC) $(CFLAGS) appl=l_racpdv dotfle=$(TD)l_racpdv.fle

$(TD)l_racprp.fle:  l_racprp.lgc
    $(CC) $(CFLAGS) appl=l_racprp dotfle=$(TD)l_racprp.fle

$(TD)lskdopzb.fle:  lskdopzb.lgc
    $(CC) $(CFLAGS) appl=lskdopzb dotfle=$(TD)lskdopzb.fle

$(TD)sk_dopzb.fle:  sk_dopzb.lgc
    $(CC) $(CFLAGS) appl=sk_dopzb dotfle=$(TD)sk_dopzb.fle

$(TD)lioszag.fle:  lioszag.lgc
    $(CC) $(CFLAGS) appl=lioszag dotfle=$(TD)lioszag.fle

$(TD)lioskar.fle:  lioskar.lgc
    $(CC) $(CFLAGS) appl=lioskar dotfle=$(TD)lioskar.fle

$(TD)lopomzag.fle:  lopomzag.lgc
    $(CC) $(CFLAGS) appl=lopomzag dotfle=$(TD)lopomzag.fle

$(TD)lopomzagt.fle:  lopomzagt.lgc
    $(CC) $(CFLAGS) appl=lopomzagt dotfle=$(TD)lopomzagt.fle

$(TD)lopomkar.fle:  lopomkar.lgc
    $(CC) $(CFLAGS) appl=lopomkar dotfle=$(TD)lopomkar.fle

$(TD)pdv_zag.fle:  pdv_zag.lgc
    $(CC) $(CFLAGS) appl=pdv_zag dotfle=$(TD)pdv_zag.fle

$(TD)pdv_zag2.fle:  pdv_zag2.lgc
    $(CC) $(CFLAGS) appl=pdv_zag2 dotfle=$(TD)pdv_zag2.fle

$(TD)pdv_zag3.fle:  pdv_zag3.lgc
    $(CC) $(CFLAGS) appl=pdv_zag3 dotfle=$(TD)pdv_zag3.fle

$(TD)opz_stat.fle:  opz_stat.lgc
    $(CC) $(CFLAGS) appl=opz_stat dotfle=$(TD)opz_stat.fle

$(TD)opz_stat_rac.fle:  opz_stat_rac.lgc
    $(CC) $(CFLAGS) appl=opz_stat_rac dotfle=$(TD)opz_stat_rac.fle

$(TD)lopz_storna.fle:  lopz_storna.lgc
    $(CC) $(CFLAGS) appl=lopz_storna dotfle=$(TD)lopz_storna.fle

$(TD)opz_ozn_dok.fle:  opz_ozn_dok.lgc
    $(CC) $(CFLAGS) appl=opz_ozn_dok dotfle=$(TD)opz_ozn_dok.fle

$(TD)lopz_stat.fle:  lopz_stat.lgc
    $(CC) $(CFLAGS) appl=lopz_stat dotfle=$(TD)lopz_stat.fle

$(TD)lrek_po_kto.fle:  lrek_po_kto.lgc
    $(CC) $(CFLAGS) appl=lrek_po_kto dotfle=$(TD)lrek_po_kto.fle
    
$(TD)sk_ugovori.fle:  sk_ugovori.lgc
    $(CC) $(CFLAGS) appl=sk_ugovori dotfle=$(TD)sk_ugovori.fle

$(TD)sk_ugovori_st.fle:  sk_ugovori_st.lgc
    $(CC) $(CFLAGS) appl=sk_ugovori_st dotfle=$(TD)sk_ugovori_st.fle

$(TD)lsk_ugovor.fle:  lsk_ugovor.lgc
    $(CC) $(CFLAGS) appl=lsk_ugovor dotfle=$(TD)lsk_ugovor.fle

$(TD)hpa_opom.fle:  hpa_opom.lgc
    $(CC) $(CFLAGS) appl=hpa_opom dotfle=$(TD)hpa_opom.fle

$(TD)lopomhpa.fle:  lopomhpa.lgc
    $(CC) $(CFLAGS) appl=lopomhpa dotfle=$(TD)lopomhpa.fle

$(TD)lopomhpa_vd.fle:  lopomhpa_vd.lgc
    $(CC) $(CFLAGS) appl=lopomhpa_vd dotfle=$(TD)lopomhpa_vd.fle

$(TD)lsk_kam_rek.fle:  lsk_kam_rek.lgc
    $(CC) $(CFLAGS) appl=lsk_kam_rek dotfle=$(TD)lsk_kam_rek.fle

$(TD)pdv_stav.fle:  pdv_stav.lgc
    $(CC) $(CFLAGS) appl=pdv_stav dotfle=$(TD)pdv_stav.fle

$(TD)lpdv_obr.fle:  lpdv_obr.lgc
    $(CC) $(CFLAGS) appl=lpdv_obr dotfle=$(TD)lpdv_obr.fle

$(TD)lpdvk_obr.fle:  lpdvk_obr.lgc
    $(CC) $(CFLAGS) appl=lpdvk_obr dotfle=$(TD)lpdvk_obr.fle

$(TD)sve_pokrij.fle:  sve_pokrij.lgc
    $(CC) $(CFLAGS) appl=sve_pokrij dotfle=$(TD)sve_pokrij.fle

$(TD)lsve_pokrij.fle:  lsve_pokrij.lgc
    $(CC) $(CFLAGS) appl=lsve_pokrij dotfle=$(TD)lsve_pokrij.fle

$(TD)sk_lstr2.fle:  sk_lstr2.lgc
    $(CC) $(CFLAGS) appl=sk_lstr2 dotfle=$(TD)sk_lstr2.fle

$(TD)lsklstr2.fle:  lsklstr2.lgc
    $(CC) $(CFLAGS) appl=lsklstr2 dotfle=$(TD)lsklstr2.fle

$(TD)lsklstr2old.fle:  lsklstr2old.lgc
    $(CC) $(CFLAGS) appl=lsklstr2old dotfle=$(TD)lsklstr2old.fle

$(TD)sktecraz.fle:  sktecraz.lgc
    $(CC) $(CFLAGS) appl=sktecraz dotfle=$(TD)sktecraz.fle

$(TD)l_tecraz.fle:  l_tecraz.lgc
    $(CC) $(CFLAGS) appl=l_tecraz dotfle=$(TD)l_tecraz.fle

$(TD)lskkomzb.fle:  lskkomzb.lgc
    $(CC) $(CFLAGS) appl=lskkomzb dotfle=$(TD)lskkomzb.fle

$(TD)sk_komzb.fle:  sk_komzb.lgc
    $(CC) $(CFLAGS) appl=sk_komzb dotfle=$(TD)sk_komzb.fle

$(TD)lskkompo.fle:  lskkompo.lgc
    $(CC) $(CFLAGS) appl=lskkompo dotfle=$(TD)lskkompo.fle

$(TD)skrndosp.fle:  skrndosp.lgc
    $(CC) $(CFLAGS) appl=skrndosp dotfle=$(TD)skrndosp.fle

$(TD)skrnndosp.fle:  skrnndosp.lgc
    $(CC) $(CFLAGS) appl=skrnndosp dotfle=$(TD)skrnndosp.fle

$(TD)lskrnndosp.fle:  lskrnndosp.lgc
    $(CC) $(CFLAGS) appl=lskrnndosp dotfle=$(TD)lskrnndosp.fle

$(TD)gt_unrns.fle:  gt_unrns.lgc
    $(CC) $(CFLAGS) appl=gt_unrns dotfle=$(TD)gt_unrns.fle

$(TD)sk_hrkomp.fle:  sk_hrkomp.lgc
    $(CC) $(CFLAGS) appl=sk_hrkomp dotfle=$(TD)sk_hrkomp.fle

$(TD)sk_izvjop.fle:  sk_izvjop.lgc
    $(CC) $(CFLAGS) appl=sk_izvjop dotfle=$(TD)sk_izvjop.fle

$(TD)l_rktraz.fle:  l_rktraz.lgc
    $(CC) $(CFLAGS) appl=l_rktraz dotfle=$(TD)l_rktraz.fle

$(TD)lnk_rpi.fle:  lnk_rpi.lgc
    $(CC) $(CFLAGS) appl=lnk_rpi dotfle=$(TD)lnk_rpi.fle

$(TD)lsk_nzktms.fle:  lsk_nzktms.lgc
    $(CC) $(CFLAGS) appl=lsk_nzktms dotfle=$(TD)lsk_nzktms.fle

$(TD)sk_idx.fle:  sk_idx.lgc
    $(CC) $(CFLAGS) appl=sk_idx dotfle=$(TD)sk_idx.fle

$(TD)lskrndosp.fle:  lskrndosp.lgc
    $(CC) $(CFLAGS) appl=lskrndosp dotfle=$(TD)lskrndosp.fle

$(TD)sk_lovir.fle:  sk_lovir.lgc
    $(CC) $(CFLAGS) appl=sk_lovir dotfle=$(TD)sk_lovir.fle

$(TD)sk_unkif.fle:  sk_unkif.lgc
    $(CC) $(CFLAGS) appl=sk_unkif dotfle=$(TD)sk_unkif.fle

$(TD)kif.fle:  kif.lgc
    $(CC) $(CFLAGS) appl=kif dotfle=$(TD)kif.fle

$(TD)sk_unkuf.fle:  sk_unkuf.lgc
    $(CC) $(CFLAGS) appl=sk_unkuf dotfle=$(TD)sk_unkuf.fle

$(TD)lkufac.fle:  lkufac.lgc
    $(CC) $(CFLAGS) appl=lkufac dotfle=$(TD)lkufac.fle

$(TD)l_tecraz_sr.fle:  l_tecraz_sr.lgc
    $(CC) $(CFLAGS) appl=l_tecraz_sr dotfle=$(TD)l_tecraz_sr.fle

$(TD)l_tecraz_su.fle:  l_tecraz_su.lgc
    $(CC) $(CFLAGS) appl=l_tecraz_su dotfle=$(TD)l_tecraz_su.fle

$(TD)skdilgar.fle:  skdilgar.lgc
    $(CC) $(CFLAGS) appl=skdilgar dotfle=$(TD)skdilgar.fle

$(TD)opca_upl.fle:  opca_upl.lgc
    $(CC) $(CFLAGS) appl=opca_upl dotfle=$(TD)opca_upl.fle

$(TD)sk_start.fle:  sk_start.lgc
    $(CC) $(CFLAGS) appl=sk_start dotfle=$(TD)sk_start.fle

$(TD)sk_default.fle:  sk_default.lgc
    $(CC) $(CFLAGS) appl=sk_default dotfle=$(TD)sk_default.fle

### MILK FILES ####

$(TD)milk_a_dd.fle:  milk_a_dd.lgc
    $(CC) $(CFLAGS) appl=milk_a_dd dotfle=$(TD)milk_a_dd.fle

$(TD)milk.fle:  milk.lgc
    $(CC) $(CFLAGS) appl=milk dotfle=$(TD)milk.fle

$(TD)milk_fact.fle:  milk_fact.lgc
    $(CC) $(CFLAGS) appl=milk_fact dotfle=$(TD)milk_fact.fle

$(TD)milk_prod.fle:  milk_prod.lgc
    $(CC) $(CFLAGS) appl=milk_prod dotfle=$(TD)milk_prod.fle

$(TD)sab_mjesta.fle:  sab_mjesta.lgc
    $(CC) $(CFLAGS) appl=sab_mjesta dotfle=$(TD)sab_mjesta.fle

$(TD)l_milk_fact.fle:  l_milk_fact.lgc
    $(CC) $(CFLAGS) appl=l_milk_fact dotfle=$(TD)l_milk_fact.fle

$(TD)l_sab_mjesta.fle:  l_sab_mjesta.lgc
    $(CC) $(CFLAGS) appl=l_sab_mjesta dotfle=$(TD)l_sab_mjesta.fle

$(TD)l_milk_prod.fle:  l_milk_prod.lgc
    $(CC) $(CFLAGS) appl=l_milk_prod dotfle=$(TD)l_milk_prod.fle

$(TD)milk_kateg.fle:  milk_kateg.lgc
    $(CC) $(CFLAGS) appl=milk_kateg dotfle=$(TD)milk_kateg.fle

$(TD)l_milk_kateg.fle:  l_milk_kateg.lgc
    $(CC) $(CFLAGS) appl=l_milk_kateg dotfle=$(TD)l_milk_kateg.fle

$(TD)l_milk_koefzn.fle:  l_milk_koefzn.lgc
    $(CC) $(CFLAGS) appl=l_milk_koefzn dotfle=$(TD)l_milk_koefzn.fle

$(TD)milk_koefzn.fle:  milk_koefzn.lgc
    $(CC) $(CFLAGS) appl=milk_koefzn dotfle=$(TD)milk_koefzn.fle

$(TD)hssc_milk.fle:  hssc_milk.lgc
    $(CC) $(CFLAGS) appl=hssc_milk dotfle=$(TD)hssc_milk.fle

$(TD)primilk_rac.fle:  primilk_rac.lgc
    $(CC) $(CFLAGS) appl=primilk_rac dotfle=$(TD)primilk_rac.fle

$(TD)primilk_pot.fle:  primilk_pot.lgc
    $(CC) $(CFLAGS) appl=primilk_pot dotfle=$(TD)primilk_pot.fle

$(TD)milk_rac.fle:  milk_rac.lgc
    $(CC) $(CFLAGS) appl=milk_rac dotfle=$(TD)milk_rac.fle

$(TD)l_milk_rac.fle:  l_milk_rac.lgc
    $(CC) $(CFLAGS) appl=l_milk_rac dotfle=$(TD)l_milk_rac.fle

$(TD)milk_racstat.fle:  milk_racstat.lgc
    $(CC) $(CFLAGS) appl=milk_racstat dotfle=$(TD)milk_racstat.fle

$(TD)lmilk_racstat.fle:  lmilk_racstat.lgc
    $(CC) $(CFLAGS) appl=lmilk_racstat dotfle=$(TD)lmilk_racstat.fle

$(TD)milk_sifra.fle:  milk_sifra.lgc
    $(CC) $(CFLAGS) appl=milk_sifra dotfle=$(TD)milk_sifra.fle

$(TD)milk_nalfakt.fle:  milk_nalfakt.lgc
    $(CC) $(CFLAGS) appl=milk_nalfakt dotfle=$(TD)milk_nalfakt.fle

$(TD)milk_count.fle:  milk_count.lgc
    $(CC) $(CFLAGS) appl=milk_count dotfle=$(TD)milk_count.fle

$(TD)milk_statkor.fle:  milk_statkor.lgc
    $(CC) $(CFLAGS) appl=milk_statkor dotfle=$(TD)milk_statkor.fle

$(TD)lmilk_cesija.fle:  lmilk_cesija.lgc
    $(CC) $(CFLAGS) appl=lmilk_cesija dotfle=$(TD)lmilk_cesija.fle

$(TD)lmilk_nalppar.fle:  lmilk_nalppar.lgc
    $(CC) $(CFLAGS) appl=lmilk_nalppar dotfle=$(TD)lmilk_nalppar.fle

$(TD)milk_potsif.fle:  milk_potsif.lgc
    $(CC) $(CFLAGS) appl=milk_potsif dotfle=$(TD)milk_potsif.fle

$(TD)milk_kpot.fle:  milk_kpot.lgc
    $(CC) $(CFLAGS) appl=milk_kpot dotfle=$(TD)milk_kpot.fle

$(TD)farme_dug.fle:  farme_dug.lgc
    $(CC) $(CFLAGS) appl=farme_dug dotfle=$(TD)farme_dug.fle

$(TD)milk_star.fle:  milk_star.lgc
    $(CC) $(CFLAGS) appl=milk_star dotfle=$(TD)milk_star.fle

$(TD)milk_usluga.fle:  milk_usluga.lgc
    $(CC) $(CFLAGS) appl=milk_usluga dotfle=$(TD)milk_usluga.fle

$(TD)l_milk_sifra.fle:  l_milk_sifra.lgc
    $(CC) $(CFLAGS) appl=l_milk_sifra dotfle=$(TD)l_milk_sifra.fle

$(TD)l_milk_usluga.fle:  l_milk_usluga.lgc
    $(CC) $(CFLAGS) appl=l_milk_usluga dotfle=$(TD)l_milk_usluga.fle

$(TD)milkdbutl.fle:  milkdbutl.lgc
    $(CC) $(CFLAGS) appl=milkdbutl dotfle=$(TD)milkdbutl.fle

$(TD)gt_punrn.fle:  gt_punrn.lgc
    $(CC) $(CFLAGS) appl=gt_punrn dotfle=$(TD)gt_punrn.fle

$(TD)skchk_gk_sk.fle:  skchk_gk_sk.lgc
    $(CC) $(CFLAGS) appl=skchk_gk_sk dotfle=$(TD)skchk_gk_sk.fle

$(TD)fillkif.fle:  fillkif.lgc
    $(CC) $(CFLAGS) appl=fillkif dotfle=$(TD)fillkif.fle

$(TD)prirn_cross.fle:  prirn_cross.lgc
    $(CC) $(CFLAGS) appl=prirn_cross dotfle=$(TD)prirn_cross.fle

$(TD)prigot_vw.fle:  prigot_vw.lgc
    $(CC) $(CFLAGS) appl=prigot_vw dotfle=$(TD)prigot_vw.fle

$(TD)prigots_vw.fle:  prigots_vw.lgc
    $(CC) $(CFLAGS) appl=prigots_vw dotfle=$(TD)prigots_vw.fle

$(TD)sk_prrac_vw.fle:  sk_prrac_vw.lgc
    $(CC) $(CFLAGS) appl=sk_prrac_vw dotfle=$(TD)sk_prrac_vw.fle

$(TD)sk_prgot_vw.fle:  sk_prgot_vw.lgc
    $(CC) $(CFLAGS) appl=sk_prgot_vw dotfle=$(TD)sk_prgot_vw.fle

$(TD)sk_prprm_vw.fle:  sk_prprm_vw.lgc
    $(CC) $(CFLAGS) appl=sk_prprm_vw dotfle=$(TD)sk_prprm_vw.fle

$(TD)sk_kif_ef3.fle:  sk_kif_ef3.lgc
    $(CC) $(CFLAGS) appl=sk_kif_ef3 dotfle=$(TD)sk_kif_ef3.fle

$(TD)imp_kifbr.fle:  imp_kifbr.lgc
    $(CC) $(CFLAGS) appl=imp_kifbr dotfle=$(TD)imp_kifbr.fle

$(TD)kif_efcom.fle:  kif_efcom.lgc
    $(CC) $(CFLAGS) appl=kif_efcom dotfle=$(TD)kif_efcom.fle

$(TD)sk_protp_vw.fle:  sk_protp_vw.lgc
    $(CC) $(CFLAGS) appl=sk_protp_vw dotfle=$(TD)sk_protp_vw.fle

$(TD)prigot_voz.fle:  prigot_voz.lgc
    $(CC) $(CFLAGS) appl=prigot_voz dotfle=$(TD)prigot_voz.fle

$(TD)prigots_voz.fle:  prigots_voz.lgc
    $(CC) $(CFLAGS) appl=prigots_voz dotfle=$(TD)prigots_voz.fle

$(TD)virpon.fle:  virpon.lgc
    $(CC) $(CFLAGS) appl=virpon dotfle=$(TD)virpon.fle

$(TD)reg_rnvir.fle:  reg_rnvir.lgc
    $(CC) $(CFLAGS) appl=reg_rnvir dotfle=$(TD)reg_rnvir.fle

$(TD)reg_avirm.fle:  reg_avirm.lgc
    $(CC) $(CFLAGS) appl=reg_avirm dotfle=$(TD)reg_avirm.fle

$(TD)l_vpdosp.fle:  l_vpdosp.lgc
    $(CC) $(CFLAGS) appl=l_vpdosp dotfle=$(TD)l_vpdosp.fle

$(TD)reg_virm.fle:  reg_virm.lgc
    $(CC) $(CFLAGS) appl=reg_virm dotfle=$(TD)reg_virm.fle

$(TD)reg_virms.fle:  reg_virms.lgc
    $(CC) $(CFLAGS) appl=reg_virms dotfle=$(TD)reg_virms.fle

$(TD)klasif_prst.fle:  klasif_prst.lgc
    $(CC) $(CFLAGS) appl=klasif_prst dotfle=$(TD)klasif_prst.fle

$(TD)niveliranje.fle:  niveliranje.lgc
    $(CC) $(CFLAGS) appl=niveliranje dotfle=$(TD)niveliranje.fle

$(TD)kompenzacije.fle:  kompenzacije.lgc
    $(CC) $(CFLAGS) appl=kompenzacije dotfle=$(TD)kompenzacije.fle

$(TD)lsk_komp.fle:  lsk_komp.lgc
    $(CC) $(CFLAGS) appl=lsk_komp dotfle=$(TD)lsk_komp.fle

$(TD)lioszag_vd.fle:  lioszag_vd.lgc
    $(CC) $(CFLAGS) appl=lioszag_vd dotfle=$(TD)lioszag_vd.fle

$(TD)lioszaghpa_vd.fle:  lioszaghpa_vd.lgc
    $(CC) $(CFLAGS) appl=lioszaghpa_vd dotfle=$(TD)lioszaghpa_vd.fle

$(TD)lioszaghpa.fle:  lioszaghpa.lgc
    $(CC) $(CFLAGS) appl=lioszaghpa dotfle=$(TD)lioszaghpa.fle

$(TD)lopomzag_vd.fle:  lopomzag_vd.lgc
    $(CC) $(CFLAGS) appl=lopomzag_vd dotfle=$(TD)lopomzag_vd.fle

$(TD)lopomzag_vdt.fle:  lopomzag_vdt.lgc
    $(CC) $(CFLAGS) appl=lopomzag_vdt dotfle=$(TD)lopomzag_vdt.fle

$(TD)skkomp_arh.fle:  skkomp_arh.lgc
    $(CC) $(CFLAGS) appl=skkomp_arh dotfle=$(TD)skkomp_arh.fle

$(TD)lklas_prst.fle:  lklas_prst.lgc
    $(CC) $(CFLAGS) appl=lklas_prst dotfle=$(TD)lklas_prst.fle

$(TD)prifakto_gt.fle:  prifakto_gt.lgc
    $(CC) $(CFLAGS) appl=prifakto_gt dotfle=$(TD)prifakto_gt.fle

$(TD)pribgots_vw.fle:  pribgots_vw.lgc
    $(CC) $(CFLAGS) appl=pribgots_vw dotfle=$(TD)pribgots_vw.fle

$(TD)pribgot_vw.fle:  pribgot_vw.lgc
    $(CC) $(CFLAGS) appl=pribgot_vw dotfle=$(TD)pribgot_vw.fle

$(TD)dacvw_dd.fle:  dacvw_dd.lgc
    $(CC) $(CFLAGS) appl=dacvw_dd dotfle=$(TD)dacvw_dd.fle
    @echo " ">skdbutl.ts

$(TD)skkam_fakt.fle:  skkam_fakt.lgc
    $(CC) $(CFLAGS) appl=skkam_fakt dotfle=$(TD)skkam_fakt.fle

$(TD)skrnnedosp.fle:  skrnnedosp.lgc
    $(CC) $(CFLAGS) appl=skrnnedosp dotfle=$(TD)skrnnedosp.fle

$(TD)sk_tecaj_hnb.fle:  sk_tecaj_hnb.lgc
    $(CC) $(CFLAGS) appl=sk_tecaj_hnb dotfle=$(TD)sk_tecaj_hnb.fle

$(TD)view_prst.fle:  view_prst.lgc
    $(CC) $(CFLAGS) appl=view_prst dotfle=$(TD)view_prst.fle

$(TD)crosstokris.fle:  crosstokris.lgc
    $(CC) $(CFLAGS) appl=crosstokris dotfle=$(TD)crosstokris.fle

$(TD)sk_rev.fle:  sk_rev.lgc
    $(CC) $(CFLAGS) appl=sk_rev dotfle=$(TD)sk_rev.fle

$(TD)epdv_mp.fle:  epdv_mp.lgc
    $(CC) $(CFLAGS) appl=epdv_mp dotfle=$(TD)epdv_mp.fle

$(TD)racfromziro.fle:  racfromziro.lgc
    $(CC) $(CFLAGS) appl=racfromziro dotfle=$(TD)racfromziro.fle

$(TD)sk_auto_pok.fle:  sk_auto_pok.lgc
    $(CC) $(CFLAGS) appl=sk_auto_pok dotfle=$(TD)sk_auto_pok.fle

$(TD)genddx1.fle:  genddx1.lgc
    $(CC) $(CFLAGS) appl=genddx1 dotfle=$(TD)genddx1.fle

$(TD)pdvs_zag.fle:  pdvs_zag.lgc
    $(CC) $(CFLAGS) appl=pdvs_zag dotfle=$(TD)pdvs_zag.fle
    
$(TD)pdvs_stav.fle:  pdvs_stav.lgc
    $(CC) $(CFLAGS) appl=pdvs_stav dotfle=$(TD)pdvs_stav.fle
    
$(TD)zp_zag.fle:  zp_zag.lgc
    $(CC) $(CFLAGS) appl=zp_zag dotfle=$(TD)zp_zag.fle
    
$(TD)zp_stav.fle:  zp_stav.lgc
    $(CC) $(CFLAGS) appl=zp_stav dotfle=$(TD)zp_stav.fle
    
$(TD)pdv_datumi.fle:  pdv_datumi.lgc
    $(CC) $(CFLAGS) appl=pdv_datumi dotfle=$(TD)pdv_datumi.fle
    
$(TD)list_pnb.fle:  list_pnb.lgc
    $(CC) $(CFLAGS) appl=list_pnb dotfle=$(TD)list_pnb.fle
    
$(TD)ppo_zbirno.fle:  ppo_zbirno.lgc
    $(CC) $(CFLAGS) appl=ppo_zbirno dotfle=$(TD)ppo_zbirno.fle
    
$(TD)ppo_15.fle:  ppo_15.lgc
    $(CC) $(CFLAGS) appl=ppo_15 dotfle=$(TD)ppo_15.fle
    
$(TD)l_ppo_zbirno.fle:  l_ppo_zbirno.lgc
    $(CC) $(CFLAGS) appl=l_ppo_zbirno dotfle=$(TD)l_ppo_zbirno.fle
    
$(TD)l_ppo_15.fle:  l_ppo_15.lgc
    $(CC) $(CFLAGS) appl=l_ppo_15 dotfle=$(TD)l_ppo_15.fle
    
$(TD)lozn_dok_err.fle:  lozn_dok_err.lgc
    $(CC) $(CFLAGS) appl=lozn_dok_err dotfle=$(TD)lozn_dok_err.fle
    
$(TD)lopz_err.fle:  lopz_err.lgc
    $(CC) $(CFLAGS) appl=lopz_err dotfle=$(TD)lopz_err.fle
    
$(TD)ozn_dok_err.fle:  ozn_dok_err.lgc
    $(CC) $(CFLAGS) appl=ozn_dok_err dotfle=$(TD)ozn_dok_err.fle
    
$(TD)l_pokontima.fle:  l_pokontima.lgc
    $(CC) $(CFLAGS) appl=l_pokontima dotfle=$(TD)l_pokontima.fle
    
$(TD)pdf_opom.fle:  pdf_opom.lgc
    $(CC) $(CFLAGS) appl=pdf_opom dotfle=$(TD)pdf_opom.fle
    
$(TD)ufa_pdvs.fle:  ufa_pdvs.lgc
    $(CC) $(CFLAGS) appl=ufa_pdvs dotfle=$(TD)ufa_pdvs.fle

$(TD)view_pdvs.fle:  view_pdvs.lgc
    $(CC) $(CFLAGS) appl=view_pdvs dotfle=$(TD)view_pdvs.fle
    
$(TD)view_vanbil.fle:  view_vanbil.lgc
    $(CC) $(CFLAGS) appl=view_vanbil dotfle=$(TD)view_vanbil.fle
    
$(TD)stat_izvj.fle:  stat_izvj.lgc
    $(CC) $(CFLAGS) appl=stat_izvj dotfle=$(TD)stat_izvj.fle
    
$(TD)l_view_vb.fle:  l_view_vb.lgc
    $(CC) $(CFLAGS) appl=l_view_vb dotfle=$(TD)l_view_vb.fle
    
$(TD)hpa_ios.fle:  hpa_ios.lgc
    $(CC) $(CFLAGS) appl=hpa_ios dotfle=$(TD)hpa_ios.fle
    
$(TD)ura_xml.fle:  ura_xml.lgc
    $(CC) $(CFLAGS) appl=ura_xml dotfle=$(TD)ura_xml.fle
    
$(TD)r2_racuni.fle:  r2_racuni.lgc
    $(CC) $(CFLAGS) appl=r2_racuni dotfle=$(TD)r2_racuni.fle
    
$(TD)akj_load.fle:  akj_load.lgc
    $(CC) $(CFLAGS) appl=akj_load dotfle=$(TD)akj_load.fle
    
$(TD)ak_racuni.fle:  ak_racuni.lgc
    $(CC) $(CFLAGS) appl=ak_racuni dotfle=$(TD)ak_racuni.fle
    
$(TD)akj_racuni_st.fle:  akj_racuni_st.lgc
    $(CC) $(CFLAGS) appl=akj_racuni_st dotfle=$(TD)akj_racuni_st.fle
    
$(TD)l_akrnnpl.fle:  l_akrnnpl.lgc
    $(CC) $(CFLAGS) appl=l_akrnnpl dotfle=$(TD)l_akrnnpl.fle
    
$(TD)akj_ugrupe.fle:  akj_ugrupe.lgc
    $(CC) $(CFLAGS) appl=akj_ugrupe dotfle=$(TD)akj_ugrupe.fle
    
$(TD)akj_grupe.fle:  akj_grupe.lgc
    $(CC) $(CFLAGS) appl=akj_grupe dotfle=$(TD)akj_grupe.fle
    
####
