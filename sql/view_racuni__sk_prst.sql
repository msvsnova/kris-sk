DROP VIEW racuni__sk_prst;

CREATE VIEW racuni__sk_prst AS (


SELECT R.cppar, R.rac_br, R.corg, R.vr_sk, R.ind_sk, R.godina, R.cvalute, R.rac_pdv_osn, R.rac_pdv_neop, R.rac_iznv, R.rac_salv, P.*
FROM sk_prst AS P 
JOIN 
(SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, * 
 FROM racuni) 
 AS R
  ON (R.cracupl = P.cracupl));
