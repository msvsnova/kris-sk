DROP VIEW skklasif_sum_err;

CREATE VIEW skklasif_sum_err AS (
SELECT sk_klasif, 
	CAST(SUM(rac_id) AS DECIMAL(15,2)) AS sumrac_id,
	CAST(SUM(rac_ip) AS DECIMAL(15,2)) AS sumrac_ip, 
	CAST(SUM(rac_saldo) AS DECIMAL(15,2)) AS sumrac_saldo,
	CAST(SUM(greska) AS DECIMAL(15,2)) AS sumgreska,
	CAST(SUM(cnt) AS INTEGER) AS sumcnt
FROM skprst__sum_pr_sk_pok
GROUP BY sk_klasif);
