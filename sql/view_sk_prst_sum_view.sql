DROP VIEW sk_prst_sum_view;

CREATE VIEW sk_prst_sum_view AS (
SELECT cracupl, prorac_indkto, sum(rac_id) AS sumid, sum(rac_ip) AS sumip, sum(rac_saldo) AS sumsaldo
FROM sk_prst 
GROUP BY cracupl, prorac_indkto
);
