DROP VIEW prskpok_sum30
go


CREATE VIEW prskpok_sum30 AS (
SELECT cracupl, CAST(SUM(pr_pok_izn) AS DECIMAL(15,2)) AS sum30pok FROM pr_sk_pok WHERE prorac_indkto = 30 GROUP BY cracupl, prorac_indkto)
go




DROP VIEW prskpok_sum20
go


CREATE VIEW prskpok_sum20 AS (
SELECT cracupl, CAST(SUM(pr_pok_izn) AS DECIMAL(15,2)) AS sum20pok FROM pr_sk_pok WHERE prorac_indkto = 20 GROUP BY cracupl, prorac_indkto)
go




DROP VIEW prskpok_sum10
go


CREATE VIEW prskpok_sum10 AS (
SELECT cracupl, CAST(SUM(pr_pok_izn) AS DECIMAL(15,2)) AS sum10pok FROM pr_sk_pok WHERE prorac_indkto = 10 GROUP BY cracupl, prorac_indkto)
go




DROP VIEW sk_pok_sum
go


CREATE VIEW sk_pok_sum AS (
SELECT corg, cppar, vr_sk, cvalute, cracuna, CAST(SUM(pok_iznv) AS DECIMAL(15,2)) sumskpok FROM sk_pok GROUP BY corg, cppar, vr_sk, cvalute, cracuna)
go





DROP VIEW sk_prst_sum_view
go


CREATE VIEW sk_prst_sum_view AS (
SELECT cracupl, prorac_indkto, sum(rac_id) AS sumid, sum(rac_ip) AS sumip, sum(rac_saldo) AS sumsaldo FROM sk_prst GROUP BY cracupl, prorac_indkto )
go




DROP VIEW sk_prst_sum10
go


CREATE VIEW sk_prst_sum10 AS (
SELECT cracupl, CAST( sumid AS DECIMAL(15,2)) AS sum10id,
		CAST( sumip AS DECIMAL(15,2)) AS sum10ip,
		CAST( sumsaldo AS DECIMAL(15,2)) AS sum10saldo FROM sk_prst_sum_view WHERE prorac_indkto = 10 )
        go




DROP VIEW sk_prst_sum20
go


CREATE VIEW sk_prst_sum20 AS (
SELECT cracupl, CAST( sumid AS DECIMAL(15,2)) AS sum20id,
		CAST( sumip AS DECIMAL(15,2)) AS sum20ip,
		CAST( sumsaldo AS DECIMAL(15,2)) AS sum20saldo FROM sk_prst_sum_view WHERE prorac_indkto = 20 )
        go




DROP VIEW sk_prst_sum30
go


CREATE VIEW sk_prst_sum30 AS (
SELECT cracupl, CAST( sumid AS DECIMAL(15,2)) AS sum30id,
		CAST( sumip AS DECIMAL(15,2)) AS sum30ip,
		CAST( sumsaldo AS DECIMAL(15,2)) AS sum30saldo FROM sk_prst_sum_view WHERE prorac_indkto = 30 )
        go




DROP VIEW racuni__sum_skprst
go


CREATE VIEW racuni__sum_skprst AS (
SELECT R.cracupl, R.cppar, R.rac_br, R.corg, R.vr_sk, R.ind_sk, R.godina, R.rac_izn, R.rac_iznv, R.ozn_dok, R.cvalute, R.tecaj, R.rac_salv,R.arh_br, R.arh_dat, R.cracuna, R.rac_predp_uk, R.rac_pdv_neop, R.rac_pdv_osn, R.rac_pdv_por, R.rac_pp010_uk, R.rac_010_osn,R.rac_010_por,R.rac_05_por,R.rac_pp010_ne,R.rac_predp_ne,
		coalesce(sum10id,0) AS sum10id, coalesce(sum10ip,0) AS sum10ip, coalesce(sum10saldo,0) AS sum10saldo,
	    coalesce(sum20id,0) AS sum20id, coalesce(sum20ip,0) AS sum20ip, coalesce(sum20saldo,0) AS sum20saldo,
	    coalesce(sum30id,0) AS sum30id, coalesce(sum30ip,0) AS sum30ip, coalesce(sum30saldo,0) AS sum30saldo FROM (SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(8)) AS cracupl, * FROM racuni)  AS R LEFT OUTER JOIN sk_prst_sum10 AS P10 ON (R.cracupl = P10.cracupl) LEFT OUTER JOIN sk_prst_sum20 AS P20 ON (R.cracupl = P20.cracupl) LEFT OUTER JOIN sk_prst_sum30 AS P30 ON (R.cracupl = P30.cracupl) )
        go




DROP VIEW racuni__sum_hpa_skprst_err
go


CREATE VIEW racuni__sum_hpa_skprst_err AS ( SELECT *, CAST((rac_iznv - sum10id - sum10ip) AS DECIMAL(15,2)) AS sum10err,
	  CAST((rac_salv - sum10saldo) AS DECIMAL(15,2)) AS sum10saldo_err,
	  CAST((rac_pdv_osn + rac_pdv_neop - sum20id - sum20ip) AS DECIMAL(15,2)) AS sum20err, 
	  CAST((rac_pdv_osn + rac_pdv_neop - sum30id - sum30ip) AS DECIMAL(15,2)) AS sum30err FROM racuni__sum_skprst )
      go





DROP VIEW racuni__sum_skprst_err
go


CREATE VIEW racuni__sum_skprst_err AS (
SELECT *, CAST((rac_izn - sum10id - sum10ip - rac_pdv_por - rac_010_por - rac_05_por) AS DECIMAL(15,2)) AS sum10err,
	  CAST((rac_salv - sum10saldo) AS DECIMAL(15,2)) AS sum10saldo_err,
	  CAST((rac_pdv_osn + rac_pdv_neop - sum20id - sum20ip) AS DECIMAL(15,2)) AS sum20err, 
	  CAST((rac_pdv_osn + rac_pdv_neop - sum30id - sum30ip) AS DECIMAL(15,2)) AS sum30err FROM racuni__sum_skprst )
      go





DROP VIEW racuni_check_pok
go


CREATE VIEW racuni_check_pok AS (
SELECT R.*, coalesce(S.sumskpok,0) AS sumskpok, coalesce(P10.sum10pok,0) AS sum10pok, coalesce(P20.sum20pok,0) AS sum20pok, coalesce(P30.sum30pok,0) AS sum30pok FROM racuni__sum_skprst AS R LEFT OUTER JOIN sk_pok_sum AS S ON (R.corg = S.corg AND R.cppar = S.cppar AND R.vr_sk = S.vr_sk AND R.cvalute = S.cvalute AND R.cracuna = S.cracuna) LEFT OUTER JOIN prskpok_sum10 AS P10 ON (R.cracupl = P10.cracupl) LEFT OUTER JOIN prskpok_sum20 AS P20 ON (R.cracupl = P20.cracupl) LEFT OUTER JOIN prskpok_sum30 AS P30 ON (R.cracupl = P30.cracupl))
go





DROP VIEW skprst_kto_sum
go


CREATE VIEW skprst_kto_sum AS (
SELECT R.vr_sk, S.sk_klasif, S.prorac_indkto, CAST(SUM(rac_id) AS DECIMAL(15,2)) AS sumrac_id, CAST(SUM(rac_ip) AS DECIMAL(15,2)) AS sumrac_ip, CAST(SUM(rac_saldo) AS DECIMAL(15,2)) AS sumrac_saldo FROM sk_prst AS S INNER JOIN (SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, *  FROM racuni)  AS R ON (R.cracupl = S.cracupl)
-- WHERE arh_br > 0
GROUP BY R.vr_sk, S.sk_klasif, S.prorac_indkto)
go





DROP VIEW skprst_kto_sum2014
go


CREATE VIEW skprst_kto_sum2014 AS (
SELECT R.vr_sk, S.sk_klasif, S.prorac_indkto, CAST(SUM(rac_id) AS DECIMAL(15,2)) AS sumrac_id, CAST(SUM(rac_ip) AS DECIMAL(15,2)) AS sumrac_ip, CAST(SUM(rac_saldo) AS DECIMAL(15,2)) AS sumrac_saldo FROM sk_prst AS S INNER JOIN (SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, *  FROM racuni)  AS R ON (R.cracupl = S.cracupl)  WHERE
-- arh_br > 0 AND
  godina = '2014'
GROUP BY R.vr_sk, S.sk_klasif, S.prorac_indkto)
go






DROP VIEW skprst_kto_sumPS
go


CREATE VIEW skprst_kto_sumPS AS (
SELECT R.vr_sk, S.sk_klasif, S.prorac_indkto, CAST(SUM(rac_id) AS DECIMAL(15,2)) AS sumrac_id, CAST(SUM(rac_ip) AS DECIMAL(15,2)) AS sumrac_ip, CAST(SUM(rac_saldo) AS DECIMAL(15,2)) AS sumrac_saldo FROM sk_prst AS S INNER JOIN (SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, *  FROM racuni)  AS R ON (R.cracupl = S.cracupl)  WHERE arh_br = 0 GROUP BY R.vr_sk, S.sk_klasif, S.prorac_indkto)
go






DROP VIEW skprst_kto_sum_knj
go


CREATE VIEW skprst_kto_sum_knj AS (
SELECT R.vr_sk, S.sk_klasif, S.prorac_indkto, CAST(SUM(rac_id) AS DECIMAL(15,2)) AS sumrac_id, CAST(SUM(rac_ip) AS DECIMAL(15,2)) AS sumrac_ip, CAST(SUM(rac_saldo) AS DECIMAL(15,2)) AS sumrac_saldo FROM sk_prst AS S INNER JOIN (SELECT RA.godina + '-' + RTRIM(RA.vr_sk) + '-' + RTRIM(RA.arh_corg) + '-' + CAST(RA.arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, RA.vr_sk  FROM racuni AS RA INNER JOIN dnev_knj AS D ON (RA.vr_sk = D.vr_sk AND RA.arh_corg = D.arh_corg AND RA.godina = D.godina AND RA.arh_br = D.arh_br)
  WHERE RA.arh_br > 0 AND ind_dk = 'R' AND brtem != '')  AS R ON (R.cracupl = S.cracupl ) GROUP BY R.vr_sk, S.sk_klasif, S.prorac_indkto)
  go





DROP VIEW kkar_kto_sum2014
go


CREATE VIEW kkar_kto_sum2014 AS (
SELECT brojkonta, CAST(SUM(st_id) AS DECIMAL(15,2)) AS sumid, CAST(SUM(st_ip) AS DECIMAL(15,2)) sumip FROM kkartice WHERE dat_knjiz > '2014-01-01' AND dat_knjiz < '2015-01-01'
GROUP BY brojkonta)
go





DROP VIEW kkar_kto_sumPS2014
go


CREATE VIEW kkar_kto_sumPS2014 AS (
SELECT brojkonta, CAST(SUM(st_id) AS DECIMAL(15,2)) AS sumid, CAST(SUM(st_ip) AS DECIMAL(15,2)) sumip FROM kkartice WHERE dat_knjiz = '2014-01-01'
GROUP BY brojkonta)
go





DROP VIEW kkar01tem_kto_sum
go


CREATE VIEW kkar01tem_kto_sum AS (
SELECT brojkonta, CAST(SUM(st_id) AS DECIMAL(15,2)) AS sum01id, CAST(SUM(st_ip) AS DECIMAL(15,2)) sum01ip FROM kkartice WHERE dat_knjiz > '2014-01-01' AND dat_knjiz < '2015-01-01' AND br_tem like '2014-01%'
GROUP BY brojkonta)
go






DROP VIEW kkar06tem_kto_sum
go


CREATE VIEW kkar06tem_kto_sum AS (
SELECT brojkonta, CAST(SUM(st_id) AS DECIMAL(15,2)) AS sum06id, CAST(SUM(st_ip) AS DECIMAL(15,2)) sum06ip FROM kkartice WHERE dat_knjiz > '2014-01-01' AND dat_knjiz < '2015-01-01' AND br_tem like '2014-06%'
GROUP BY brojkonta)
go





DROP VIEW kkar_upltem_kto_sum
go


CREATE VIEW kkar_upltem_kto_sum AS (
SELECT brojkonta, CAST(SUM(st_id) AS DECIMAL(15,2)) AS sumid, CAST(SUM(st_ip) AS DECIMAL(15,2)) sumip FROM kkartice WHERE dat_knjiz > '2014-01-01' 
 AND (br_tem like '%-02-%' OR br_tem like '%-12-%' OR br_tem like '%-21-%' OR br_tem like '%-22-%' OR br_tem like '%-23-%') GROUP BY brojkonta)
 go







DROP VIEW skprst__kkar_kto_sum
go


CREATE VIEW skprst__kkar_kto_sum AS (
SELECT * FROM
skprst_kto_sum AS S INNER JOIN kkar_kto_sum AS K ON (S.sk_klasif = K.brojkonta))
go




DROP VIEW skprst__kkar_err
go


CREATE VIEW skprst__kkar_err AS (
SELECT T.*,
CAST(CASE prorac_indkto 
				WHEN 20 THEN (sumid - (sumrac_ip - sumrac_saldo))
				WHEN 30 THEN sumid 
				WHEN 10 THEN (sumid - sumrac_id)
				ELSE 9999999999
     END
AS DECIMAL(15,2)) AS greska_id,
CAST(CASE prorac_indkto 
				WHEN 20 THEN (sumip - sumrac_ip)
				WHEN 30 THEN (sumip - (sumrac_ip - sumrac_saldo)) 
				WHEN 10 THEN (sumip - (sumrac_id - sumrac_saldo))
				ELSE 9999999999
     END
AS DECIMAL(15,2)) AS greska_ip
FROM skprst__kkar_kto_sum AS T)
go




DROP VIEW uplate__pr_sk_pok
go


CREATE VIEW uplate__pr_sk_pok AS (
SELECT U.*, P.cracupl, P.rbr_prst, P.pr_pok_izn, P.cracuna, P.ind_pok, P.prorac_indkto FROM uplate AS U LEFT OUTER JOIN pr_sk_pok AS P ON (U.cuplate = P.cuplate AND U.vr_sk = P.vr_sk AND U.cppar = P.cppar AND U.cvalute = P.cvalute))
go





DROP VIEW uplate__sum10pok_err
go


CREATE VIEW uplate__sum10pok_err AS (
SELECT U.*, CAST((U.upl_iznv - U.upl_salv - UP.sumpok) AS DECIMAL(15,2)) AS greska ,UP.sumpok, UP.cnt FROM uplate AS U INNER JOIN (
	SELECT P.cuplate, P.vr_sk, P.cppar, P.cvalute, 
		coalesce(CAST(SUM(pr_pok_izn) AS DECIMAL(15,2)),0) AS sumpok,
		count(pr_pok_izn) AS cnt
	FROM uplate__pr_sk_pok AS P
	WHERE P.prorac_indkto = 10
	GROUP BY P.cuplate, P.vr_sk, P.cppar, P.cvalute
) AS UP ON (U.cuplate = UP.cuplate AND U.vr_sk = UP.vr_sk AND U.cppar = UP.cppar AND U.cvalute = UP.cvalute))
go




DROP VIEW skprst__sum_pr_sk_pok
go


CREATE VIEW skprst__sum_pr_sk_pok AS (

SELECT P.*, CAST((P.rac_id+P.rac_ip - P.rac_saldo - SP.sumpok) AS DECIMAL(15,2)) AS greska, SP.sumpok, SP.cnt FROM sk_prst AS P INNER JOIN (
	SELECT sk_prst.cracupl, 
	sk_prst.rbr_prst,
	sk_prst.prorac_indkto,
	CAST( coalesce( sum(pr_sk_pok.pr_pok_izn), 0) AS DECIMAL(15,2) ) AS sumpok,
	count(pr_sk_pok.pr_pok_izn)	 AS cnt
	FROM sk_prst    LEFT OUTER JOIN    pr_sk_pok ON (sk_prst.cracupl = pr_sk_pok.cracupl AND sk_prst.rbr_prst = pr_sk_pok.rbr_prst)
	GROUP BY sk_prst.cracupl, sk_prst.rbr_prst, sk_prst.prorac_indkto ) AS SP ON (P.cracupl = SP.cracupl AND P.rbr_prst = SP.rbr_prst) )
    go




DROP VIEW stanje_avansa
go


CREATE VIEW stanje_avansa AS (
SELECT corg, cppar, sum(upl_iznv) av_saldo FROM uplate where vr_sk='O' 
GROUP BY corg, cppar)
go




