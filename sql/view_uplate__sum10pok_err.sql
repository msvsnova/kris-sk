DROP VIEW uplate__sum10pok_err;

CREATE VIEW uplate__sum10pok_err AS (
SELECT U.*, CAST((U.upl_iznv - U.upl_salv - UP.sumpok) AS DECIMAL(15,2)) AS greska ,UP.sumpok, UP.cnt
FROM uplate AS U INNER JOIN (
	SELECT P.cuplate, P.vr_sk, P.cppar, P.cvalute, 
		coalesce(CAST(SUM(pr_pok_izn) AS DECIMAL(15,2)),0) AS sumpok,
		count(pr_pok_izn) AS cnt
	FROM uplate__pr_sk_pok AS P
	WHERE P.prorac_indkto = 10
	GROUP BY P.cuplate, P.vr_sk, P.cppar, P.cvalute
) AS UP ON (U.cuplate = UP.cuplate AND U.vr_sk = UP.vr_sk AND U.cppar = UP.cppar AND U.cvalute = UP.cvalute));
