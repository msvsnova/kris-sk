--DROP VIEW stizv__uplate__racuni
--;GO
CREATE VIEW stizv__uplate__racuni AS (
SELECT U.*, SP.ind_pok, SP.pok_izn, SP.pok_iznv, SP.tec_raz, R.br_kifkuf, R.caruspro, R.corg_prst, R.cracuna, R.cvrkifkuf, R.pdv_balans, R.rac_010_osn, R.rac_010_por, R.rac_br, R.rac_dat, R.rac_dosp, R.rac_ext_br, R.rac_ind_avans, R.rac_izn, R.rac_iznv, R.rac_kam, R.rac_npl, R.rac_opis, R.rac_pdv_izv, R.rac_pdv_kosn, R.rac_pdv_kpor, R.rac_pdv_mpr, R.rac_pdv_neop, R.rac_pdv_nula, R.rac_pdv_osn, R.rac_pdv_ost, R.rac_pdv_por, R.rac_pdv_tuz, R.rac_pdv_yes, R.rac_pp010_ne, R.rac_pp010_ok, R.rac_pp010_uk, R.rac_predp_ne, R.rac_predp_ok, R.rac_predp_uk, R.rac_rbr, R.rac_rsal, R.rac_rsalv, R.rac_sal, R.rac_salv, R.rbr_ziro, R.stopa_pdv, R.vir_pnb3, R.vir_pnb4
FROM stizv__uplate AS U INNER JOIN sk_pok AS SP ON (U.cuplate = SP.cuplate AND U.cvalute = SP.cvalute AND U.corg = SP.corg AND U.cppar = SP.cppar AND U.vr_sk = SP.vr_sk)
 INNER JOIN racuni AS R ON (SP.cracuna = R.cracuna AND SP.cvalute = R.cvalute AND SP.corg = R.corg AND SP.cppar = R.cppar AND SP.vr_sk = R.vr_sk)
 )
 --;GO
