DROP VIEW skklasif_dokUstr_sum_err;

CREATE VIEW skklasif_dokUstr_sum_err AS (
/*
	Prikazuje sumarno UNIJU racuna i STORNO grupirano po sk_klasif
*/
SELECT sk_klasif, 
	CAST(SUM(rac_id) AS DECIMAL(15,2)) AS sumrac_id,
	CAST(SUM(rac_ip) AS DECIMAL(15,2)) AS sumrac_ip, 
	CAST(SUM(rac_saldo) AS DECIMAL(15,2)) AS sumrac_saldo,
	CAST(SUM(greska) AS DECIMAL(15,2)) AS sumgreska,
	CAST(SUM(cnt) AS INTEGER) AS sumcnt
FROM dokUstr_sum_prskpok GROUP BY sk_klasif);
