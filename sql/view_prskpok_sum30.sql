DROP VIEW prskpok_sum30;

CREATE VIEW prskpok_sum30 AS (
SELECT cracupl, CAST(SUM(pr_pok_izn) AS DECIMAL(15,2)) AS sum30pok
FROM pr_sk_pok 
WHERE prorac_indkto = 30
GROUP BY cracupl, prorac_indkto);
