DROP VIEW sk_prst_sum20;

CREATE VIEW sk_prst_sum20 AS (
SELECT cracupl, CAST( sumid AS DECIMAL(15,2)) AS sum20id,
		CAST( sumip AS DECIMAL(15,2)) AS sum20ip,
		CAST( sumsaldo AS DECIMAL(15,2)) AS sum20saldo
FROM sk_prst_sum_view WHERE prorac_indkto = 20
);
