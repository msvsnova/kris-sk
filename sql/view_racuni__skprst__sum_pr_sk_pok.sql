DROP VIEW racuni__skprst__sum_pr_sk_pok;

CREATE VIEW racuni__skprst__sum_pr_sk_pok AS (
select R.cppar, R.rac_br, R.corg, R.vr_sk, R.ind_sk, R.godina, R.rac_izn, R.rac_iznv, R.ozn_dok, R.cvalute, R.tecaj, R.rac_salv,
           R.arh_br, R.arh_dat, R.cracuna, R.rac_predp_uk, R.rac_pdv_neop, R.rac_pdv_osn, R.rac_pdv_por, R.rac_pp010_uk, R.rac_010_osn, R.rac_010_por, S.* from skprst__sum_pr_sk_pok AS S INNER JOIN 
(SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, * 
 FROM racuni) 
 AS R
  ON (R.cracupl = S.cracupl));
