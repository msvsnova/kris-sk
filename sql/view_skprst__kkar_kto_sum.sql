DROP VIEW skprst__kkar_kto_sum;

CREATE VIEW skprst__kkar_kto_sum AS (
SELECT * FROM
skprst_kto_sum_knj AS S INNER JOIN kkar_kto_sum AS K ON (S.sk_klasif = K.brojkonta))
;
