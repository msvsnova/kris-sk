DROP VIEW skprst_kto_sum;

CREATE VIEW skprst_kto_sum AS (
SELECT R.vr_sk, S.sk_klasif, CAST(SUM(rac_id) AS DECIMAL(15,2)) AS sumrac_id, CAST(SUM(rac_ip) AS DECIMAL(15,2)) AS sumrac_ip, CAST(SUM(rac_saldo) AS DECIMAL(15,2)) AS sumrac_saldo
FROM sk_prst AS S INNER JOIN 
(SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, * 
 FROM racuni) 
 AS R ON (R.cracupl = S.cracupl)
GROUP BY R.vr_sk, S.sk_klasif);
