DROP VIEW racuni__sum_sk_pok;

CREATE VIEW racuni__sum_sk_pok AS (
SELECT R.*, SP.sumpok, SP.cnt FROM racuni AS R INNER JOIN 
(
	SELECT R.corg, R.cppar, R.vr_sk, R.cvalute, R.cracuna, CAST(sum(sk_pok.pok_iznv) AS DECIMAL(15,2)) AS sumpok, count(*) as cnt
	FROM racuni AS R INNER JOIN sk_pok ON (R.corg = sk_pok.corg AND R.cppar = sk_pok.cppar AND R.cvalute = sk_pok.cvalute AND R.cracuna = sk_pok.cracuna )
	GROUP BY R.corg, R.cppar, R.vr_sk, R.cvalute, R.cracuna
) AS SP
ON (R.corg = SP.corg AND R.cppar = SP.cppar AND R.cvalute = SP.cvalute AND R.cracuna = SP.cracuna )
);
