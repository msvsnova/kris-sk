--DROP VIEW stizv__uplate
--;GO
CREATE VIEW stizv__uplate AS (
SELECT S.br_izvoda, S.br_zirorn, S.brojdepozita, S.brojkonta, S.corg, S.dat_izvr, S.dep_ddosp, S.godina_izv, S.ind_podstavke, S.iz_br_rac, S.iz_cppar, S.iz_naz_pp, S.iz_reklam, S.iz_sjed, S.iz_svrha, S.izv_cvalute, S.izv_pvr_id, S.izv_pvr_ip, S.izv_val_ind, S.izv_val_izn, S.izv_val_tecaj, S.pbr_izvoda, S.pbr_odb1, S.pbr_odb2, S.pbr_zad1, S.pbr_zad2, S.prorac_indkto, S.rbr_naloga, S.sk_konto, S.st_iz_id, S.st_iz_ip, S.tecaj, U.arh_br, U.arh_corg, U.arh_dat, U.cppar, U.cuplate, U.cvalute, U.godina, U.ind_sk, U.knjizeno, U.ozn_dok, U.sk_klasif, U.upl_br, U.upl_dat, U.upl_dosp, U.upl_izn, U.upl_iznv, U.upl_npl, U.upl_opis, U.upl_rsal, U.upl_rsalv, U.upl_sal, U.upl_salv, U.vr_sk
FROM st_izvod AS S INNER JOIN dkizv__uplate AS U ON (S.godina_izv = U.godina AND S.br_zirorn = U.br_zirorn AND S.br_izvoda = U.br_izvoda AND S.pbr_izvoda = U.pbr_izvoda AND S.rbr_naloga = U.rbr_naloga)
WHERE pbr_naloga = 0
)
--;GO
