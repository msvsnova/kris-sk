DROP VIEW str_skprst__sum_prskpok;

CREATE VIEW str_skprst__sum_prskpok AS (
/*
	STORNO RACUNI - VEZA PREMA pr_sk_pok se ostvaruje preko CUPLATE 
	Prikazuje skprst od STORNO RACUNA sa pridruzenim poljima sumpok (suma pokrivanja)
	cnt (broj slogova u pr_sk_pok) i greska (iznos - saldo - sumpok) koja mora biti 0 
*/
SELECT R.vr_sk, P.*, CAST((P.rac_id+P.rac_ip - P.rac_saldo - SP.sumpok) AS DECIMAL(15,2)) AS greska, SP.sumpok, SP.cnt FROM sk_prst AS P INNER JOIN (
	SELECT sk_prst.cracupl, 
	sk_prst.rbr_prst,
	sk_prst.prorac_indkto,
	CAST( coalesce( sum(pr_sk_pok.pr_pok_izn), 0) AS DECIMAL(15,2) ) AS sumpok,
	count(pr_sk_pok.pr_pok_izn)	 AS cnt
	FROM sk_prst    LEFT OUTER JOIN    pr_sk_pok ON (sk_prst.cracupl =    pr_sk_pok.cuplate    AND sk_prst.rbr_prst = pr_sk_pok.rbr_prst)
	GROUP BY sk_prst.cracupl, sk_prst.rbr_prst, sk_prst.prorac_indkto ) AS SP 
ON (P.cracupl = SP.cracupl AND P.rbr_prst = SP.rbr_prst) INNER JOIN (
		SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, * 
		 FROM racuni WHERE ind_sk = 'S') AS R
ON (R.cracupl = P.cracupl)
);
