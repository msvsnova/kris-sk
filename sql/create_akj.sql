DROP INDEX dbo.akj_racuni.ixnwt5aa;
DROP TABLE dbo.akj_racuni;

CREATE TABLE dbo.akj_racuni
(
   akj_rac_id        int,
   akj_lid           varchar(100),
   akj_broj          int,
   akj_br_dok        varchar(100),
   akj_cfakture      varchar(100),
   akj_sasija        varchar(100),
   akj_bruto         decimal(15,2),
   akj_ces           varchar(100),
   akj_cestarina     decimal(15,2),
   akj_cst_pl        decimal(15,2),
   akj_cst_cvh       varchar(100),
   akj_cst_hak       varchar(100),
   akj_cst_izn       decimal(15,2),
   akj_cst_mup       varchar(100),
   akj_cst_pp        varchar(100),
   akj_cst_stat      int,
   akj_cst_zup       int,
   akj_datum         varchar(100),
   akj_datum_f       varchar(100),
   akj_dosp          varchar(100),
   akj_datf_poc      varchar(100),
   akj_pnb           varchar(100),
   akj_datf_kraj     varchar(100),
   akj_godina        varchar(100),
   akj_jir           varchar(100),
   akj_marka         varchar(200),
   akj_nstor         int,
   akj_nstor_id      int,
   akj_nstor_lid     int,
   akj_nstor_time    varchar(100),
   akj_netto         decimal(15,2),
   akj_rjesenje      varchar(100),
   akj_ospdv         int,
   akj_ospdv_razlog  varchar(100),
   akj_porez         decimal(15,2),
   akj_neop          decimal(15,2),
   akj_osnovica      decimal(15,2),
   akj_stopa         decimal(4,2),
   akj_plist         decimal(15,2),
   akj_kpc_id        int,
   akj_kpc_kbr       varchar(100),
   akj_kpc_lid       int,
   akj_kpc_mbr       varchar(100),
   akj_kpc_mj        varchar(100),
   akj_kpc_mj_ozn    varchar(100),
   akj_kpc_ime       varchar(100),
   akj_kpc_oib       varchar(100),
   akj_kpc_opc       varchar(100),
   akj_kpc_opc_ozn   varchar(100),
   akj_kpc_ptt       varchar(100),
   akj_kpc_adr       varchar(100),
   akj_kpc_vrsta     varchar(100),
   akj_kpc_zupa      varchar(100),
   akj_kpc_zupa_ozn  varchar(100),
   akj_cmv_izn       decimal(15,2),
   akj_cmv_zupa_ozn  varchar(100),
   akj_ref_ime       varchar(100),
   akj_ref_lic       varchar(100),
   akj_regozn        varchar(100),
   akj_stanica       varchar(100),
   akj_stanica_ozn   varchar(100),
   akj_stanica_rbr   varchar(100),
   akj_tip_vozila    varchar(100),
   akj_vrijeme       varchar(100),
   akj_vrdok         varchar(100),
   akj_vrrac         varchar(100),
   akj_zki           varchar(100)
);
CREATE UNIQUE CLUSTERED INDEX ixq14fav
   ON T_AKJ.dbo.akj_racuni (akj_rac_id ASC);
   

DROP INDEX dbo.akj_racuni.ixq14iaa;
IF OBJECT_ID('dbo.akj_racuni_st', 'U') IS NOT NULL DROP TABLE dbo.akj_racuni_st;
CREATE TABLE dbo.akj_racuni_st
(
   akjst_bruto    decimal(15,2)   NULL,
   akjst_cijena   decimal(15,2)   NULL,
   akjst_id       int             NULL,
   akjst_kol      int             NULL,
   akj_lid        varchar(100)    NULL,
   akjst_neto     decimal(15,2)   NULL,
   akjst_porez    decimal(15,2)   NULL,
   akjst_stopa    decimal(4,2)    NULL,
   akjst_pcvh     decimal(15,2)   NULL,
   akjst_phak     decimal(15,2)   NULL,
   akjst_pmup     decimal(15,2)   NULL,
   akjst_pstp     decimal(15,2)   NULL,
   akj_rac_id     int             NULL,
   akjst_rnlid    smallint        NULL,
   akjst_usl_id   int             NULL,
   akjst_usl_naz  varchar(250)    NULL,
   akjst_usl_ozn  int             NULL
);

CREATE UNIQUE CLUSTERED INDEX ixq14iaa
   ON AKJ.dbo.akj_racuni_st (akjst_id ASC, akj_rac_id ASC, akjst_usl_ozn ASC);


DROP INDEX dbo.akj_npl.ixy21gaa;
DROP TABLE dbo.akj_npl;

CREATE TABLE dbo.akj_npl
(
   aknpl_brrata  int         NULL,
   aknpl_bruto   decimal(15,2)    NULL,
   aknpl_id      int              NULL,
   aknpl_lid     smallint         NULL,
   aknpl_npl     char(1)     NULL,
   aknpl_neto    decimal(15,2)    NULL,
   aknpl_porez   decimal(15,2)    NULL,
   akjst_id   int              NULL,
   aknpl_st_lid  int         NULL
);

CREATE UNIQUE CLUSTERED INDEX ixy21gaa
   ON AKJ.dbo.akj_npl (aknpl_id ASC, akjst_id ASC);

CREATE TABLE dbo.ak_racuni
(
   akj_rac_id       int             NOT NULL,
   ak_lid           char(3)         NOT NULL,
   ak_broj          int             NOT NULL,
   ak_br_dok        char(22)        NOT NULL,
   ak_cfakture      char(16)        NOT NULL,
   ak_sasija        char(20)        NOT NULL,
   ak_bruto         decimal(15,2)   NOT NULL,
   ak_ces           char(4)         NOT NULL,
   ak_cestarina     decimal(15,2)   NOT NULL,
   ak_cst_pl        decimal(15,2)   NOT NULL,
   ak_cst_cvh       char(2)         NOT NULL,
   ak_cst_hak       char(2)         NOT NULL,
   ak_cst_izn       decimal(15,2)   NOT NULL,
   ak_cst_mup       char(2)         NOT NULL,
   ak_cst_pp        char(2)         NOT NULL,
   ak_cst_stat      smallint        NOT NULL,
   ak_cst_zup       smallint        NOT NULL,
   ak_datum         datetime        NOT NULL,
   ak_datum_f       datetime        NOT NULL,
   ak_dosp          datetime        NOT NULL,
   ak_datf_poc      datetime        NOT NULL,
   ak_pnb           char(20)        NOT NULL,
   ak_datf_kraj     datetime        NOT NULL,
   ak_godina        char(4)         NOT NULL,
   ak_jir           char(50)        NOT NULL,
   ak_marka         char(20)        NOT NULL,
   ak_nstor         smallint        NOT NULL,
   ak_nstor_id      int             NOT NULL,
   ak_nstor_lid     smallint        NOT NULL,
   ak_nstor_time    datetime        NOT NULL,
   ak_netto         decimal(15,2)   NOT NULL,
   ak_rjesenje      char(30)        NOT NULL,
   ak_ospdv         smallint        NOT NULL,
   ak_ospdv_razlog  char(50)        NOT NULL,
   ak_porez         decimal(15,2)   NOT NULL,
   ak_neop          decimal(15,2)   NOT NULL,
   ak_osnovica      decimal(15,2)   NOT NULL,
   ak_stopa         decimal(4,2)    NOT NULL,
   ak_plist         decimal(15,2)   NOT NULL,
   ak_kpc_id        int             NOT NULL,
   ak_kpc_kbr       char(6)         NOT NULL,
   ak_kpc_lid       smallint        NOT NULL,
   ak_kpc_mbr       char(16)        NOT NULL,
   ak_kpc_mj        char(30)        NOT NULL,
   ak_kpc_mj_ozn    char(7)         NOT NULL,
   ak_kpc_ime       varchar(100)    NOT NULL,
   ak_kpc_oib       char(11)        NOT NULL,
   ak_kpc_opc       char(25)        NOT NULL,
   ak_kpc_opc_ozn   char(4)         NOT NULL,
   ak_kpc_ptt       char(5)         NOT NULL,
   ak_kpc_adr       char(50)        NOT NULL,
   ak_kpc_vrsta     char(1)         NOT NULL,
   ak_kpc_zupa      char(20)        NOT NULL,
   ak_kpc_zupa_ozn  char(2)         NOT NULL,
   ak_cmv_izn       decimal(15,2)   NOT NULL,
   ak_cmv_zupa_ozn  char(2)         NOT NULL,
   ak_ref_ime       char(60)        NOT NULL,
   ak_ref_lic       char(5)         NOT NULL,
   ak_regozn        char(10)        NOT NULL,
   ak_stanica       char(2)         NOT NULL,
   ak_stanica_ozn   char(2)         NOT NULL,
   ak_stanica_rbr   char(2)         NOT NULL,
   ak_tip_vozila    char(20)        NOT NULL,
   ak_vrijeme       datetime        NOT NULL,
   ak_vrdok         char(3)         NOT NULL,
   ak_vrrac         char(1)         NOT NULL,
   ak_zki           char(50)        NOT NULL,
   ak_preneseno     char(1)         NOT NULL
);

CREATE UNIQUE CLUSTERED INDEX ixc2y4ax
   ON AKJ.dbo.ak_racuni (akj_rac_id ASC);

DROP TABLE rac_stp;

CREATE TABLE dbo.rac_stp
(
   rac_dat       datetime        NOT NULL,
   rac_br        char(60)        NOT NULL,
   sk_cstp       smallint        NOT NULL,
   cppar         int             NOT NULL,
   ppar_mbr      char(30)        NOT NULL,
   oib           char(50)        NOT NULL,
   corg_prst     char(12)        NOT NULL,
   rac_dosp      datetime        NOT NULL,
   rac_opis      char(80)        NOT NULL,
   rac_iznv      decimal(15,2)   NOT NULL,
   ind_sk        char(2)         NOT NULL,
   ozn_dok       char(10)        NOT NULL,
   sk_klasif     char(12)        NOT NULL,
   rac_npl       char(3)         NOT NULL,
   rac_prolazno  decimal(15,2)   NOT NULL,
   rac_pdv_osn   decimal(15,2)   NOT NULL,
   rac_pdv_por   decimal(15,2)   NOT NULL,
   rac_pdv_kosn  decimal(15,2)   NOT NULL,
   rac_pdv_kpor  decimal(15,2)   NOT NULL,
   ppar_naziv    char(200)       NOT NULL,
   ppar_mj       char(30)        NOT NULL,
   adr_ppar      char(200)       NOT NULL,
   ind_prensk    char(1)         NOT NULL,
   ind_gotrac    char(1)         NOT NULL,
   arh_corg      char(12)        NOT NULL,
   ak_vrrac      char(1)         NOT NULL
);

CREATE UNIQUE CLUSTERED INDEX ixv23gax
   ON AKJ.dbo.rac_stp (rac_dat ASC, rac_br ASC, sk_cstp ASC);
