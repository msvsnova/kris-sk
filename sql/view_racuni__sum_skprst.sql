DROP VIEW racuni__sum_skprst;

CREATE VIEW racuni__sum_skprst AS (
SELECT R.*, coalesce(sum10id,0) AS sum10id, coalesce(sum10ip,0) AS sum10ip, coalesce(sum10saldo,0) AS sum10saldo,
	    coalesce(sum20id,0) AS sum20id, coalesce(sum20ip,0) AS sum20ip, coalesce(sum20saldo,0) AS sum20saldo,
	    coalesce(sum30id,0) AS sum30id, coalesce(sum30ip,0) AS sum30ip, coalesce(sum30saldo,0) AS sum30saldo
FROM
(SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(8)) AS cracupl, * FROM racuni)  AS R
LEFT OUTER JOIN sk_prst_sum10 AS P10 ON (R.cracupl = P10.cracupl)
LEFT OUTER JOIN sk_prst_sum20 AS P20 ON (R.cracupl = P20.cracupl)
LEFT OUTER JOIN sk_prst_sum30 AS P30 ON (R.cracupl = P30.cracupl)
);
