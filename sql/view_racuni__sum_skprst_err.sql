DROP VIEW racuni__sum_skprst_err;

CREATE VIEW racuni__sum_skprst_err AS (
SELECT *, CAST((rac_iznv - sum10id - sum10ip) AS DECIMAL(15,2)) AS sum10err,
	  CAST((rac_salv - sum10saldo) AS DECIMAL(15,2)) AS sum10saldo_err,
	  CAST((rac_pdv_osn + rac_pdv_neop - sum20id - sum20ip) AS DECIMAL(15,2)) AS sum20err, 
	  CAST((rac_pdv_osn + rac_pdv_neop - sum30id - sum30ip) AS DECIMAL(15,2)) AS sum30err
FROM racuni__sum_skprst
);
