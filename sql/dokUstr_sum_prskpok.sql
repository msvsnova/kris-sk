DROP VIEW dokUstr_sum_prskpok;

CREATE VIEW dokUstr_sum_prskpok AS(
/*
	Prikazuje UNIJU SKPRST-ova sa sum_pok normalnih i STORNO racuna
*/
SELECT * FROM dok_skprst__sum_prskpok UNION 
	(SELECT * FROM str_skprst__sum_prskpok));
