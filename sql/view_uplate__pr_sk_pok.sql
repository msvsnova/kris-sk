DROP VIEW uplate__pr_sk_pok;

CREATE VIEW uplate__pr_sk_pok AS (
SELECT U.*, P.cracupl, P.rbr_prst, P.pr_pok_izn, P.cracuna, P.ind_pok, P.prorac_indkto
FROM 
uplate AS U INNER JOIN pr_sk_pok AS P ON (U.cuplate = P.cuplate AND U.vr_sk = P.vr_sk AND U.cppar = P.cppar AND U.cvalute = P.cvalute));
