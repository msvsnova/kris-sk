
SELECT * from racuni_check_pok 
where vr_sk='K' 
and sum10id + sum10ip != rac_iznv
go

--chk: odgovara li saldo 10ki saldu ra�una
SELECT * from racuni_check_pok 
where vr_sk='D' 
and rac_salv!=0
and sum10saldo != rac_salv
go
--chk: suma salda 10ki odgovara saldu ra�una koji imaju saldo
SELECT * from racuni_check_pok 
where vr_sk='K' 
and rac_salv!=0
and sum10saldo != rac_salv
go
--chk: suma salda 10ki odgovara saldu ra�una koji nemaju saldo
SELECT * from racuni_check_pok 
where vr_sk='K' 
and rac_salv=0
and sum10saldo != 0
go
--chk: ispravan iznos 20ki i 30ki (rac_ip)
SELECT * from racuni__sum_hpa_skprst_err
where  vr_sk = 'K' and 
 arh_br!=0 and
 (sum20err != 0 or sum30err != 0)
ORDER by cppar, cracupl
go

--chk: imamo li 20ke za ra�une s osnovicom na ra�unu
select * from racuni__sum_hpa_skprst_err
where vr_sk='K' and arh_br!=0
and sum10saldo!=0 and rac_pdv_osn!=0 and sum20ip=0
go
--chk: suma 20ki odgovara sumi rac_pdv_osn + rac_pdv_neop
SELECT * from racuni_check_pok 
where vr_sk='K' 
and sum20id + sum20ip !=0 and arh_br!=0
and sum20id + sum20ip != rac_pdv_osn + rac_pdv_neop 
go

--chk: dobavlja�i u sk_prst
SELECT * from racuni_check_pok 
where vr_sk='D' and arh_br!=0
and (sum10ip != rac_iznv or
sum10saldo != rac_salv )
go

--chk: sve 20ke imaju saldo gdje 10ke imaju saldo kupci
SELECT * from racuni_check_pok 
where sum10saldo>0.01 and vr_sk='K' 
and arh_br!=0
and (sum20saldo =0 OR sum30saldo=0)
go

--chk: sve 20ke imaju saldo gdje 10ke imaju saldo kupci
SELECT * from racuni_check_pok 
where sum10saldo<-0.01 and vr_sk='K' 
and arh_br!=0
and (sum20saldo =0 OR sum30saldo=0)
go

--chk: sve 20ke imaju saldo gdje 10ke imaju saldo dobavlja�i
SELECT * from racuni_check_pok 
where sum10saldo!=0 and vr_sk='D' 
--and arh_br!=0 
and sum30saldo=0
go
--chk: iznosi 10ki odgovaraju iznosu ra�una kupaca
select * from racuni_check_pok 
where vr_sk='K' and sum10id != rac_iznv
go
--chk: iznosi 10ki odgovaraju iznosu ra�una dobavlja�a
select * from racuni_check_pok 
where vr_sk='D' and sum10ip != rac_iznv
go
--chk: ispravnost ind_sk
select * from racuni where rac_iznv>0 and ind_sk='S'
go
select * from racuni where rac_iznv<0 and ind_sk='D'
go
select * from uplate where upl_iznv>0 and ind_sk='S'
go
select * from uplate where upl_iznv<0 and ind_sk='D'
go

/* PROVJERA POKRIVANJA   */


SELECT * from racuni_check_pok 
where vr_sk='K' 
and sum10id + sum10ip != rac_iznv
go