DROP VIEW skprst__pr_sk_pok;

CREATE VIEW skprst__pr_sk_pok AS (
SELECT sk_prst.cracupl, 
	sk_prst.rbr_prst,
	corg_prst,
	sk_klasif,
	rac_id,
	rac_ip,
	rac_indkto,
	rac_saldo,
	caruspro,
	cfakture,
	fak_st_rbr,
	rba_komit,
	ckta_key,
	sk_prst.prorac_indkto,
	pr_sk_pok.pr_pok_izn
FROM sk_prst INNER JOIN pr_sk_pok ON (sk_prst.cracupl = pr_sk_pok.cracupl AND sk_prst.rbr_prst = pr_sk_pok.rbr_prst));
