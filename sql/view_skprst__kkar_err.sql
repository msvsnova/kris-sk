DROP VIEW skprst__kkar_err;

CREATE VIEW skprst__kkar_err AS (
SELECT T.*, 
CAST(CASE prorac_indkto 
				WHEN 20 THEN (sumid - (sumrac_ip - sumrac_saldo))
				WHEN 30 THEN sumid 
				WHEN 10 THEN (sumid - sumrac_id)
				ELSE 9999999999
     END
AS DECIMAL(15,2)) AS greska_id,
CAST(CASE prorac_indkto 
				WHEN 20 THEN (sumip - sumrac_ip)
				WHEN 30 THEN (sumip - (sumrac_ip - sumrac_saldo)) 
				WHEN 10 THEN (sumip - (sumrac_id - sumrac_saldo))
				ELSE 9999999999
     END
AS DECIMAL(15,2)) AS greska_ip
FROM skprst__kkar_kto_sum AS T);
