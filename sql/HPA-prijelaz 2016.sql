
/* UTIL: A�uriranje salda sk_prst dob. 2015 */

SELECT * from racuni_check_pok 
where vr_sk='K' 
and sum10id + sum10ip != rac_iznv
go


update sk_prst set rac_saldo=25.4 where cracupl='2015-K-1-0-84263' and rbr_prst=1
update sk_prst set rac_saldo=-1626.81 where cracupl='2015-K-1-140-1' and rbr_prst=1
update sk_prst set rac_saldo=-1301.45 where cracupl='2015-K-1-140-1' and rbr_prst>1
update sk_prst set rac_saldo=0.58 where cracupl='2015-K-1-205-3692' and rbr_prst=1
update sk_prst set rac_saldo=0.46 where cracupl='2015-K-1-205-3692' and rbr_prst=2
update sk_prst set rac_saldo=0.46 where cracupl='2015-K-1-205-3692' and rbr_prst=3
update sk_prst set rac_saldo=0.14 where cracupl='2015-K-1-205-3692' and rbr_prst=4
update sk_prst set rac_saldo=0.11 where cracupl='2015-K-1-205-3692' and rbr_prst=5
update sk_prst set rac_saldo=0.11 where cracupl='2015-K-1-205-3692' and rbr_prst=6
update sk_prst set rac_saldo=235.41 where cracupl='2015-K-1-66-2' and rbr_prst=1
update sk_prst set rac_saldo=188.33 where cracupl='2015-K-1-66-2' and rbr_prst=2
update sk_prst set rac_saldo=188.33 where cracupl='2015-K-1-66-2' and rbr_prst=3
update sk_prst set rac_saldo=6.20 where cracupl='2015-K-1-66-2' and rbr_prst=4
update sk_prst set rac_saldo=4.96 where cracupl='2015-K-1-66-2' and rbr_prst=5
update sk_prst set rac_saldo=4.96 where cracupl='2015-K-1-66-2' and rbr_prst=6
update sk_prst set rac_saldo=rac_ip+rac_id where cracupl='2016-K-1-41-35' 
update sk_prst set rac_saldo=rac_saldo-0.01 where cracupl='2015-K-1-180-2612'
update sk_prst set rac_saldo=rac_ip+rac_id where cracupl='2016-K-1-46-5' 
update sk_prst set rac_saldo=rac_ip+rac_id where cracupl='2016-K-1-73-1'
update sk_prst set rac_saldo=3122.4 where cracupl='2015-K-1-0-148'
update sk_prst set rac_saldo=273.87 where cracupl='2015-K-1-0-1863' and rbr_prst=1
update sk_prst set rac_saldo=219.1 where cracupl='2015-K-1-0-1863' and rbr_prst=2
update sk_prst set rac_saldo=219.1 where cracupl='2015-K-1-0-1863' and rbr_prst=3
update sk_prst set rac_saldo=671.59 where cracupl='2015-K-1-0-208' and rbr_prst=1
update sk_prst set rac_saldo=rac_ip+rac_id where cracupl='2016-K-1-68-8' 

SELECT * from racuni_check_pok 
where vr_sk='K' 
and rac_salv!=0
and sum10saldo != rac_salv
go
--bkp here

update sk_prst set rac_saldo=rac_ip where prorac_indkto!=10 
and cracupl like '2015-K-1-%' and cracupl in
(select cracupl from racuni_check_pok
where cracupl like '2015-K-1-%' and sum10saldo!=0 and sum20ip!=0
and sum20saldo=0 and sum10id=sum10saldo)

update sk_prst set rac_saldo=0 where cracupl='2016-D-1-12-3'
go
update sk_prst 
set rac_saldo=24950.44 
where cracupl='2015-D-1-165-9' and prorac_indkto=10
go
update sk_prst 
set rac_saldo=22679.35, rac_id=22679.35
where cracupl='2015-D-1-165-9' and prorac_indkto=30
go
update sk_prst set rac_saldo=-1626.81
where cracupl='2015-K-1-140-1' and prorac_indkto=10
go
update sk_prst set rac_saldo=0.01 where cracupl='2015-K-1-0-35626'
and rbr_prst between 1 and 3
go
update sk_prst set rac_saldo=0 where cracupl='2015-K-1-0-35626'
and rbr_prst between 4 and 9
go
update sk_prst set rac_saldo=0 where cracupl='2016-K-1-41-40'
go
update sk_prst set rac_saldo=0 where cracupl='2016-K-1-38-1790'
go
update sk_prst set rac_saldo=0 where cracupl='2016-K-1-38-115'
go

/* UTIL: A�uriranje salda 20/30 sk_prst */

--chk: suma salda 10ki odgovara saldu ra�una koji imaju saldo
SELECT * from racuni_check_pok 
where vr_sk='K' 
and rac_salv!=0
and sum10saldo != rac_salv
go
--chk: suma salda 10ki odgovara saldu ra�una koji nemaju saldo
SELECT * from racuni_check_pok 
where vr_sk='K' 
and rac_salv=0
and sum10saldo != 0
go

--chk: ispravan iznos 20ki i 30ki (rac_ip)
SELECT * from racuni__sum_hpa_skprst_err
where  vr_sk = 'K' and 
 arh_br!=0 and
 (sum20err != 0 or sum30err != 0)
ORDER by cppar, cracupl
go

--chk: imamo li 20ke za ra�une s osnovicom na ra�unu
select * from racuni__sum_hpa_skprst_err
where vr_sk='K' and arh_br!=0
and sum10saldo!=0 and rac_pdv_osn!=0 and sum20ip=0
go

--chk: suma 20ki odgovara sumi rac_pdv_osn + rac_pdv_neop
SELECT * from racuni_check_pok 
where vr_sk='K' 
and sum20id + sum20ip !=0 and arh_br!=0
and sum20id + sum20ip != rac_pdv_osn + rac_pdv_neop 
go

--chk: dobavlja�i u sk_prst
SELECT * from racuni_check_pok 
where vr_sk='D' and arh_br!=0
and (sum10ip != rac_iznv or
sum10saldo != rac_salv )
go

--chk: sve 20ke imaju saldo gdje 10ke imaju saldo kupci
SELECT * from racuni_check_pok 
where sum10saldo!=0 and vr_sk='K' and arh_br!=0
and (sum20saldo =0 OR sum30saldo=0)
go
--chk: sve 20ke imaju saldo gdje 10ke imaju saldo dobavlja�i
SELECT * from racuni_check_pok 
where sum10saldo!=0 and vr_sk='D' and arh_br!=0 
and sum30saldo=0
go
--chk: iznosi 10ki odgovaraju iznosu ra�una kupaca
select * from racuni_check_pok 
where vr_sk='K' and sum10id != rac_iznv
go
--chk: iznosi 10ki odgovaraju iznosu ra�una dobavlja�a
select * from racuni_check_pok 
where vr_sk='D' and sum10ip != rac_iznv
go

update sk_pok
set cracuna='2015-Otpis potra�ivanja-11-12-13-14', cuplate='2015-Otpis potra�ivanja1-11-12-13-14', ind_pok='SR'
where cppar=7949 and cracuna='2015-Otpis potra�ivanja1-11-12-13-14'
go
update racuni set rac_salv=-1626.81 
where cppar=21014 and rac_br='7485' and godina='2015'
go
update sk_pok 
set cuplate='2016-313' 
where cppar=16582 and cracuna='2015-18758'
go
update sk_pok 
set cuplate='2016-1137' 
where cppar=91 and cracuna='2015-19670'
go
update sk_pok 
set cuplate='2016-379' 
where cppar=8179 and cracuna='2015-18229'
go
update sk_pok 
set cuplate='2016-679' 
where cppar=16974 and cracuna='2015-15969'
go
update sk_pok 
set cuplate='2016-386' 
where cppar=20423 and cracuna='2015-18222'
go
update sk_pok 
set cuplate='2016-314' 
where cppar=20773 and cracuna='2015-16432'
go
update sk_pok 
set cuplate='2016-380' 
where cppar=21841 and cracuna='2015-18228'
go
update sk_pok 
set cuplate='2016-387' 
where cppar=5095176 and cracuna='2015-18221'
go
update sk_pok 
set cuplate='2016-385' 
where cppar=5237774 and cracuna='2015-18223'
go
update sk_pok 
set cuplate='2016-381' 
where cppar=5298272 and cracuna='2015-18227'
go
update sk_pok 
set cuplate='2016-1133' 
where cppar=5299678 and cracuna='2015-17626'
go
update sk_pok 
set cuplate='2016-382' 
where cppar=6033660 and cracuna='2015-18226'
go
update sk_pok 
set cuplate='2016-383' 
where cppar=6036713 and cracuna='2015-18225'
go
update sk_pok 
set cuplate='2016-390' 
where cppar=6036718 and cracuna='2015-18218'
go
update sk_pok 
set cuplate='2016-680' 
where cppar=6043418 and cracuna='2015-17673'
go
update sk_pok 
set cuplate='2016-391' 
where cppar=6057212 and cracuna='2015-18217'
go
update sk_pok 
set cuplate='2016-389' 
where cppar=6066845 and cracuna='2015-18219'
go
update sk_pok 
set cuplate='2016-388' 
where cppar=6074642 and cracuna='2015-18220'
go
update sk_pok 
set cuplate='2016-1794' 
where cppar=6086561 and cracuna='2015-19398'
go
update sk_pok 
set cuplate='2016-384' 
where cppar=6122583 and cracuna='2015-18224'
go
update racuni set rac_salv=68.75 where cppar=19 and rac_br='2714'
go
delete from sk_pok where cppar=19 and cuplate='2016-2767'
go

--chk: ispravnost ind_sk
select * from racuni where rac_iznv>0 and ind_sk='S'
go
select * from racuni where rac_iznv<0 and ind_sk='D'
go
select * from uplate where upl_iznv>0 and ind_sk='S'
go
select * from uplate where upl_iznv<0 and ind_sk='D'
go

/* PROVJERA POKRIVANJA   */



