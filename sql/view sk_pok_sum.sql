DROP VIEW sk_pok_sum;

CREATE VIEW sk_pok_sum AS (
SELECT corg, cppar, vr_sk, cvalute, cracuna, CAST(SUM(pok_iznv) AS DECIMAL(15,2)) sumskpok
FROM sk_pok
GROUP BY corg, cppar, vr_sk, cvalute, cracuna);
