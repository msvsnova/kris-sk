--DROP VIEW dkizv__uplate
--;GO

CREATE VIEW dkizv__uplate AS (
SELECT D.sk_zirorn AS br_zirorn, D.upl_izvod AS br_izvoda, D.upl_pbrizv AS pbr_izvoda, U.*
FROM dnev_knj AS D INNER JOIN uplate AS U ON (D.vr_sk = U.vr_sk AND D.arh_corg = U.arh_corg AND D.godina = U.godina AND D.arh_br = U.arh_br)
WHERE D.ind_dk = 'U'
)
--;GO
