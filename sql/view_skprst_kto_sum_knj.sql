DROP VIEW skprst_kto_sum_knj;

CREATE VIEW skprst_kto_sum_knj AS (
SELECT R.vr_sk, S.sk_klasif, S.prorac_indkto, CAST(SUM(rac_id) AS DECIMAL(15,2)) AS sumrac_id, CAST(SUM(rac_ip) AS DECIMAL(15,2)) AS sumrac_ip, CAST(SUM(rac_saldo) AS DECIMAL(15,2)) AS sumrac_saldo
FROM sk_prst AS S INNER JOIN 
(SELECT RA.godina + '-' + RTRIM(RA.vr_sk) + '-' + RTRIM(RA.arh_corg) + '-' + CAST(RA.arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, RA.vr_sk 
 FROM racuni AS RA INNER JOIN dnev_knj AS D ON (RA.vr_sk = D.vr_sk AND RA.arh_corg = D.arh_corg AND RA.godina = D.godina AND RA.arh_br = D.arh_br) 
  WHERE RA.arh_br > 0 AND ind_dk = 'R' AND brtem != '') 
 AS R ON (R.cracupl = S.cracupl )
GROUP BY R.vr_sk, S.sk_klasif, S.prorac_indkto);
