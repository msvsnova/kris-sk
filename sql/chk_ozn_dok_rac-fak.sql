SELECT racuni.ozn_dok,
       fakture.ozn_dok,
       racuni.rac_dat,
       fakture.rac_dat,
       racuni.rac_br,
       fakture.rac_br,
       racuni.cppar,
       fakture.cppar,
       racuni.vr_sk,
       fakture.vr_sk,
       racuni.godina,
       fakture.godina,
       *
FROM racuni
  JOIN fakture
    ON (racuni.rac_dat = fakture.rac_dat
   AND racuni.rac_br = fakture.rac_br
   AND racuni.cppar = fakture.cppar
   AND racuni.vr_sk = fakture.vr_sk
   AND racuni.godina = fakture.godina)
WHERE racuni.godina = 2017 
--and racuni.rac_dat between '2017/10/01' and '2017/10/31'
AND   racuni.ozn_dok != fakture.ozn_dok
order by racuni.cppar, racuni.rac_dat