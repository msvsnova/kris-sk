

BEGIN TRANSACTION;
UPDATE sk_prst SET rac_saldo =0
FROM sk_prst AS S INNER JOIN 
	(SELECT *
	FROM racuni__sk_prst
	WHERE vr_sk = 'K' AND rac_salv = 0 and prorac_indkto=10  AND rac_saldo != 0) AS R
ON (S.cracupl = R.cracupl AND S.rbr_prst = R.rbr_prst);

COMMIT;



SELECT *
	FROM racuni__sk_prst
	WHERE vr_sk = 'K' AND rac_salv = 0 and prorac_indkto=10  AND rac_saldo != 0;


SELECT sk_klasif, sum(rac_saldo)
	FROM racuni__sk_prst
	WHERE vr_sk = 'K' AND rac_salv = 0 and prorac_indkto=10  AND rac_saldo != 0
	GROUP BY sk_klasif;
