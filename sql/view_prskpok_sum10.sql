DROP VIEW prskpok_sum10;

CREATE VIEW prskpok_sum10 AS (
SELECT cracupl, CAST(SUM(pr_pok_izn) AS DECIMAL(15,2)) AS sum10pok
FROM pr_sk_pok 
WHERE prorac_indkto = 10
GROUP BY cracupl, prorac_indkto);
