DROP VIEW racuni_check_pok;

CREATE VIEW racuni_check_pok AS (
SELECT R.*, coalesce(S.sumskpok,0) AS sumskpok, coalesce(P10.sum10pok,0) AS sum10pok, coalesce(P20.sum20pok,0) AS sum20pok, coalesce(P30.sum30pok,0) AS sum30pok
FROM racuni__sum_skprst AS R LEFT OUTER JOIN sk_pok_sum AS S ON (R.corg = S.corg AND R.cppar = S.cppar AND R.vr_sk = S.vr_sk AND R.cvalute = S.cvalute AND R.cracuna = S.cracuna)
LEFT OUTER JOIN prskpok_sum10 AS P10 ON (R.cracupl = P10.cracupl)
LEFT OUTER JOIN prskpok_sum20 AS P20 ON (R.cracupl = P20.cracupl)
LEFT OUTER JOIN prskpok_sum30 AS P30 ON (R.cracupl = P30.cracupl));
