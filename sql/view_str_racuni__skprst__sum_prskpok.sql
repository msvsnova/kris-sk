DROP VIEW str_racuni__skprst__sum_pr_sk_pok;

CREATE VIEW str_racuni__skprst__sum_pr_sk_pok AS (
/*
	STORNO racuni povezani sa sumarnim pokrivanjima sk_prst-a
	pr_sk_pok povezan preko cuplate u slucaju storna
*/
SELECT R.cppar, R.rac_br, R.corg, R.ind_sk, R.godina, R.rac_izn, R.rac_iznv, R.ozn_dok, R.cvalute, R.tecaj, R.rac_salv,
           R.arh_br, R.arh_dat, R.cracuna, R.rac_predp_uk, R.rac_pdv_neop, R.rac_pdv_osn, R.rac_pdv_por, R.rac_pp010_uk, R.rac_010_osn, R.rac_010_por, S.* FROM str_skprst__sum_prskpok AS S INNER JOIN 
(SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, * 
 FROM racuni WHERE racuni.ind_sk = 'S') 
 AS R
  ON (R.cracupl = S.cracupl));
