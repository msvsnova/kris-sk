DROP VIEW sk_prst_sum30;

CREATE VIEW sk_prst_sum30 AS (
SELECT cracupl, CAST( sumid AS DECIMAL(15,2)) AS sum30id,
		CAST( sumip AS DECIMAL(15,2)) AS sum30ip,
		CAST( sumsaldo AS DECIMAL(15,2)) AS sum30saldo
FROM sk_prst_sum_view WHERE prorac_indkto = 30
);
