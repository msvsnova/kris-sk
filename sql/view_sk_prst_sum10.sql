DROP VIEW sk_prst_sum10;

CREATE VIEW sk_prst_sum10 AS (
SELECT cracupl, CAST( sumid AS DECIMAL(15,2)) AS sum10id,
		CAST( sumip AS DECIMAL(15,2)) AS sum10ip,
		CAST( sumsaldo AS DECIMAL(15,2)) AS sum10saldo
FROM sk_prst_sum_view WHERE prorac_indkto = 10
);
