DROP VIEW racuni__sum102030;

CREATE VIEW racuni__sum102030 AS (
SELECT R.*, sum10id, sum10ip, sum10saldo,
	    sum20id, sum20ip, sum20saldo,
	    sum30id, sum30ip, sum30saldo
FROM
(SELECT godina + '-' + RTRIM(vr_sk) + '-' + RTRIM(arh_corg) + '-' + CAST(arh_br AS VARCHAR(10)) +  '-' + CAST(rac_rbr AS VARCHAR(10)) AS cracupl, * FROM racuni)  AS R
LEFT OUTER JOIN sk_prst_sum10 AS P10 ON (R.cracupl = P10.cracupl)
LEFT OUTER JOIN sk_prst_sum20 AS P20 ON (R.cracupl = P20.cracupl)
LEFT OUTER JOIN sk_prst_sum30 AS P30 ON (R.cracupl = P30.cracupl)
);
