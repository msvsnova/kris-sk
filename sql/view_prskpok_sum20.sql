DROP VIEW prskpok_sum20;

CREATE VIEW prskpok_sum20 AS (
SELECT cracupl, CAST(SUM(pr_pok_izn) AS DECIMAL(15,2)) AS sum20pok
FROM pr_sk_pok 
WHERE prorac_indkto = 20
GROUP BY cracupl, prorac_indkto);
